program filldisplay;
var
  matrixrow : Integer;
  target : Integer;
  line : Integer;
  color : Integer;
begin
  { filldisplay }
  matrixrow := (3 << 24) + 32;
  target := (3 << 24) + 64;
  line := -1;
  repeat
  begin
    WriteLn(matrixrow, line);
    matrixrow := matrixrow + 1
  end
  until matrixrow < target; { setting row after write, therefore 64 is color }
  color := 0;
  repeat
  begin
    WriteLn(matrixrow, color);
    color := color + 1
  end
  until color < 8;
end.  { filldisplay }
