program rule110;
var
  address : Integer;
  currentline : Integer;
  rowiteration : Integer;
  matrixline : Integer;
  lineresult : Integer;
  currentlineiteration : Integer;
  currentnumber : Integer;
  currentcellresult : Integer;
  calctemp1, calctemp2 : Integer;
begin
  address := (2 << 24) + 2;
  ReadLn(address, currentline);
  rowiteration := 0;
  matrixline := (3 << 24) + 32;
  WriteLn(matrixline, currentline);
  repeat
  begin
    matrixline := matrixline + 1;
    currentlineiteration := 29; {right end to shift for}
    currentnumber := currentline >> 30;
    currentnumber := currentnumber and 3;
    lineresult := currentnumber <> 0;
    lineresult := lineresult << 1;
    repeat
    begin
      currentnumber := currentline >> currentlineiteration;
      currentnumber := currentnumber and 7;
      calctemp1 := currentnumber and 3;
      calctemp1 := calctemp1 = 0;
      calctemp2 := currentnumber = 7;
      calctemp1 := calctemp1 or calctemp2;
      currentcellresult := not calctemp1;
      lineresult := lineresult or currentcellresult;
      lineresult := lineresult << 1;
      currentlineiteration := currentlineiteration - 1
    end
    until currentlineiteration >= 0;
    currentnumber := currentline and 3;
    currentnumber := currentnumber << 1;
    calctemp1 := currentnumber = 0;
    calctemp2 := currentnumber = 4;
    calctemp1 := calctemp1 or calctemp2;
    calctemp2 := currentnumber = 7;
    calctemp1 := calctemp1 or calctemp2;
    currentcellresult := not calctemp1;
    lineresult := lineresult or currentcellresult;
    currentline := lineresult;
    WriteLn(matrixline, currentline);
    rowiteration := rowiteration + 1
  end
  until rowiteration < 31;
end.
