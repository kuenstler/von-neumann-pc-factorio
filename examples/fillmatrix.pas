program fillmatrix;
var
  matrixrow : Integer;
  rowtarget: Integer;
  line: Integer;
begin
  { fillmatrix }
  matrixrow := (3 << 24) + (32 * 3);
  rowtarget := (3 << 24) + (32 * 4);
  line := 3 | (4 << 3) | (5 << 6) | (1 << 9) | (7 << 12) | (4 << 15) | (2 << 18) | (0 << 21) | (4 << 24) | (6 << 27);
  WriteLn(rowtarget, 0);
  repeat
  begin
    WriteLn(matrixrow, line);
    matrixrow := matrixrow + 1;
  end
  until matrixrow < rowtarget;
  rowtarget := rowtarget + 1;
  WriteLn(rowtarget, 1);
end.  { fillmatrix }
