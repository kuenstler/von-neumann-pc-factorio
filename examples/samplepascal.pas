program samplepascal;
var
  input1 : Integer;
  address : Integer;
  input2 : Integer;
begin
  { samplepascal }
  address := (2 << 24) + 2;
  ReadLn(address, input1);
  address := address + 1;
  ReadLn(address, input2);
  repeat
  begin
    input1 := input1 + input2;
  end
  until input1 <= 120;
  WriteLn((3 << 24) + 2, input1)
end.  { samplepascal }
