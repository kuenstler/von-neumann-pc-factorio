program hello;
var
  address: Integer;
  systemaddress: Integer;
begin { hello world! }
  address := 5 * 32;
  systemaddress := 3 << 24 | (6 * 32 - 1);
  WriteLn(systemaddress, 0);
  WriteASCII(address, 72);
  address := address + 1;
  WriteASCII(address, 69);
  address := address + 1;
  WriteASCII(address, 76);
  address := address + 1;
  WriteASCII(address, 76);
  address := address + 1;
  WriteASCII(address, 79);
  address := address + 4;
  WriteASCII(address, 87);
  address := address + 1;
  WriteASCII(address, 79);
  address := address + 1;
  WriteASCII(address, 82);
  address := address + 1;
  WriteASCII(address, 76);
  address := address + 1;
  WriteASCII(address, 68);
  address := address + 1;
  WriteASCII(address, 33);
end.  { hello world! }
