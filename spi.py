#! /usr/bin/env python3
"""SPI - Simple Pascal Interpreter. Part 19"""

import argparse
import sys

from Compiler.ASTCleanup import ASTCleanup
from Compiler.BlockStack import ProgramStackProxy
from Compiler.Compiler import Compiler
from Compiler.Error import LexerError, ParserError, SemanticError
from Compiler.FactorioBp import list_to_json, json_to_bp
from Compiler.Generators import PascalGenerator
from Compiler.Interpreter import Interpreter
from Compiler.Lexer import Lexer
from Compiler.Options import Options
from Compiler.Parser import Parser
from Compiler.SemanticAnalyzer import SemanticAnalyzer


def main():
    parser = argparse.ArgumentParser(
        description='SPI - Simple Pascal Interpreter'
    )
    parser.add_argument(
        'input_file',
        help='Pascal source file',
        type=argparse.FileType('r')
    )
    parser.add_argument(
        '--scope',
        help='Print scope information',
        action='store_true',
        dest='should_log_scope'
    )
    parser.add_argument(
        '--stack',
        help='Print call stack',
        action='store_true',
        dest='should_log_stack'
    )
    parser.add_argument(
        '--optimisations',
        help='Print AST optimisations',
        action='store_true',
        dest='should_log_optimisations'
    )
    parser.add_argument(
        '--no-newlines',
        help='Print newlines in AST-Node representations',
        action='store_false',
        dest='should_use_newlines'
    )
    parser.add_argument(
        '--show-final-ast',
        help='Print final AST',
        action='store_true',
        dest='show_final_ast'
    )
    parser.add_argument(
        '--verbose',
        help='Activate all debug options',
        action='store_true',
        dest='verbosity'
    )
    parser.add_argument(
        '--python',
        help='Output Python script',
        nargs='?',
        const='python.py',
        default=None,
        type=argparse.FileType('w'),
        dest='python_file'
    )
    parser.add_argument(
        '--pascal',
        help='Output Pascal script',
        nargs='?',
        const='pascal.pas',
        default=None,
        type=argparse.FileType('w'),
        dest='pascal_file'
    )
    parser.add_argument(
        '--bp',
        help='Output Factorio Blueprint script',
        nargs='?',
        const='blueprint.bp',
        default=None,
        type=argparse.FileType('w'),
        dest='bp_file'
    )
    parser.add_argument(
        '--bp-ints',
        help='Output the Factorio commands',
        action='store_true',
        dest='bp_integers'
    )
    parser.add_argument(
        '--ast',
        help='Output AST into file',
        nargs='?',
        const='ast.py',
        default=None,
        type=argparse.FileType('w'),
        dest='ast_file'
    )
    parser.add_argument(
        '--step',
        help='Step through instructions while interpreting',
        action='store_true',
    )
    parser.parse_args(namespace=Options)
    Options.verbose()

    text = Options.input_file.read()

    lexer = Lexer(text)
    try:
        parser = Parser(lexer)
        tree = parser.parse()
    except (LexerError, ParserError) as e:
        print(e.message)
        sys.exit(1)

    semantic_analyzer = SemanticAnalyzer()
    try:
        semantic_analyzer.visit(tree)
    except SemanticError as e:
        print(e.message)
        sys.exit(1)

    ast_cleanup = ASTCleanup()
    ast_cleanup.truth_visit(tree)

    stack = ProgramStackProxy(tree)
    stack.compute()
    tree = stack.final_tree

    if Options.show_final_ast:
        print(tree)

    if Options.ast_file:
        with Options.ast_file as f:
            f.write('from Compiler.ASTNodes import *\n')
            f.write('from Compiler.Tokens import *\n')
            f.write('\n')
            f.write('ast = ')
            f.writelines(str(tree))
            f.write('\n')

    if Options.bp_integers or Options.bp_file:
        compiler = Compiler(tree, stack)
        mixed_list = compiler.compile()
        if Options.bp_integers:
            print(mixed_list)
        if Options.bp_file:
            with Options.bp_file as f:
                json: str = list_to_json(mixed_list, f.name.replace('.bp', ''))
                bp: str = json_to_bp(json)
                f.writelines(bp)

    pascal = PascalGenerator()

    if Options.pascal_file:
        with Options.pascal_file as f:
            f.writelines(pascal.visit(tree))

    interpreter = Interpreter(tree, arguments={33554434: 1, 33554435: 10}, step=Options.step)
    interpreter.interpret()


if __name__ == '__main__':
    main()
