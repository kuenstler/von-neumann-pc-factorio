# Von Neumann PC in Factorio

The Aim of this project is to build a von-Neumann Computer in Factorio

# How to use it

On the lower left hand side is the input section.  
The first constant combiantor sets the address of the program to be run. 1 is the 2nd cell of the ROM, wich is the position of the example program. The example program loads the data from the 2nd and 3rd constant combinator from the input section, adds the second value to the first one, until the sum is above 120. Finally it displays the result at the second monitor from the top at the output section. If you turn the first comperator off, the CU will idle and wait for instructions.

# Expand RAM/ROM

In the blueprint book on this page, you will find RAM and ROM extensions. To apply them, simply build them so they overlap with a existing RAM/ROM. For the ram, you need to switch between both bps bechause the build is so large, that you need substations every 2nd row.

# Expand In/Output

If you need further In/Outputs, you will find some bps for the actual I/Os as well as number displays. Be careful with the selector numbers at the comperators.

# Build your own Programs

For a reference of my pascal compiler, look at [this page](/Doc/language.md).

# Processor specifications

For processor specifications, look into [this file](/Doc/processor.md).

# Processor design

For the design of the Factorio processor, check out [this md](/Doc/design.md)
