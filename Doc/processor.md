# Processor specifications

| Category | Size |
| ------ | ------ |
| Registers | 16 |
| Address bus | 26bit | 
| Data bus | 32bit |

Inputs, Outputs, RAM and ROM share the Address bus.

## Instructions

 - Reg: 4 bit Register address
 - Mem: 26 bit Memory address
 - RegFlag 9 bit Regflag, explained at the Flags section
 - int16: self explanatory, two's complement

The command describes only the first 2, 6 or 10 bits

 | Command | Arguments | Description | Comment |
 | ------- | --------- | ----------- | ------- |
 | 0x0 | Mem | Set program pointer to Mem | |
 | 0x1 | Reg Mem | Copy from Mem to Reg | |
 | 0x2 | Mem Reg | Copy from Reg to Mem | |
 | 0x30 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 + RegFlag2 | |
 | 0x31 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 - RegFlag2 | |
 | 0x32 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 * RegFlag2 | |
 | 0x33 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 / RegFlag2 | only integers |
 | 0x34 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 ** RegFlag2 | |
 | 0x35 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 % RegFlag2 | |
 | 0x36 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 >> RegFlag2 | |
 | 0x37 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 << RegFlag2 | |
 | 0x38 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 & RegFlag2 | |
 | 0x39 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 &#124; RegFlag2 | |
 | 0x3a | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 ^ RegFlag2 | |
 | 0x3b | RegFlag1 RegFlag2 | Compare RegFlag2 and RegFlag1 | Flag = 2 <= 1 | |
 | 0x3c | int16 RegFlag | Jump int16 steps if RegFlag |  |
 | 0x3d |  | Quit the program / free the processor | |
 | 0x3e | Reg int16 | Read value from int16 cells to Reg | compared to current pointer |
 | 0x3f0 | RegFlag1 RegFlag2 | Read value from address at RegFlag1 and write it to RegFlag2 | Don't specify an Register to write to and instead specify a number
 | 0x3f1 | RegFlag1 RegFlag2 | Read value from RegFlag2 and write it to address at RegFlag1 | |
 | 0x3f2 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 == RegFlag2 | |
 | 0x3f3 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 != RegFlag2 | |
 | 0x3f4 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 < RegFlag2 | |
 | 0x3f5 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 <= RegFlag2 | |
 | 0x3f6 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 > RegFlag2 | |
 | 0x3f7 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 >= RegFlag2 | |
 | 0x3f8 | Reg1 RegFlag2 RegFlag3 | Reg1 = RegFlag3 ^ 0xffffffff | invert every bit |
 | 0x3f9 | RegFlag1 RegFlag2 | Write the ASCII Keycode RegFlag1 to the 14-segment-display at address RegFlag2 | Only works with the 14-segment display attatched |

## Flags

### RegFlag

RegFlag means a 9bit address

Numbers are returned by a 0x100 and the number. The numbers are being interpret as two's complement number with a mask
of 0x0ff.
Flags are accessed by a 0x010 and the flag address. 

### Usual Flags

They are either changed by a calculation or by calling the comparision instruction

 | Address | Flag |
 | ------- | ---- |
 | 0x0 | A == B |
 | 0x1 | A > B |
 | 0x2 | A < B |
 | 0x3 | A >= B |
 | 0x4 | A <= B |
 | 0x5 | A != B |
 | 0x6 | A + 1 |
 | 0x7 | A - 1 |
 | 0x8 | underflow (currently unused) |
 | 0x9 | overflow |
