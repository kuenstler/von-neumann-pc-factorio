# Reference of my pascal language

## Differences to freepascal

In general I'm trying to be as close to freepascals language as possible. But in some points my language is different
 - writeln: it takes exactly 2 arguments: the address and the content to write in this order
 - readln: it takes 2 arguments: the address (for compatibility reasons as variable) and the variable to write into
 - repeat ... until uses a continuation condition

## Implemented constructs

 - program
 - begin ... end
 - variable declarations
 - assign
 - +, -, *, / (only integer results), %, **, <<, >>
 - and, or, xor, not (only the first bit), ~ (not, but all bits flipped), <, =, >, <> (not equal),
 - integers or booleans
 - if (...) then ... else ...¹
 - repeat ... until ...¹²
 - while ... do ...¹
 - break³
 - readln, writeln⁴  
 1: the compound statement is not included  
 2: unlike fpcs implementation, this is a continuation condition, it is faster than a while loop  
 3: only within loops  
 4: used to read from inputs and write to outputs

If you are interested in EBNF descriptions go to [the code](/Compiler/Parser.py#L515)

# How to learn it
 
 I've followed [this tutorial](https://ruslanspivak.com/lsbasi-part1/) until part 19 (if there are any new) and this
 compiler is based on this, but I didn't implement floats and procedures because of their complexity. Instead I've
 invented loops, conditions, readln and writeln because of their practical use for me.