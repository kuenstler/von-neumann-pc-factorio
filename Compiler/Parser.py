"""Parser for SPI"""

from Compiler.ASTNodes import *
from Compiler.Error import ParserError, ErrorCode
from Compiler.Lexer import Lexer
from Compiler.Tokens import Token, TokenType


class Parser:
    def __init__(self, lexer: Lexer):
        self.lexer: Lexer = lexer
        # set current token to the first token taken from the input
        self.current_token: Token = self.get_next_token()

    def get_next_token(self) -> Token:
        return self.lexer.get_next_token()

    def error(self, error_code: ErrorCode, token: Token):
        if token.first:
            raise ParserError(
                error_code=error_code,
                token=token,
                message=f'{error_code.value} -> {token}, maybe you forgot to put a semicolon in the previous line'
            )
        raise ParserError(
            error_code=error_code,
            token=token,
            message=f'{error_code.value} -> {token}',
        )

    def eat(self, token_type: str):
        # compare the current token type with the passed token
        # type and if they match then "eat" the current token
        # and assign the next token to the self.current_token,
        # otherwise raise an exception.
        if self.current_token.type == token_type:
            self.current_token = self.get_next_token()
        else:
            self.error(
                error_code=ErrorCode.UNEXPECTED_TOKEN,
                token=self.current_token,
            )

    def program(self, **kwargs) -> Program:
        """
        program : PROGRAM variable SEMI block DOT
        """
        token: Token = self.current_token
        self.eat(TokenType.PROGRAM)
        var_node: Var = self.variable(**kwargs)
        prog_name: str = var_node.value
        self.eat(TokenType.SEMI)
        block_node: Block = self.block(**kwargs)
        program_node: Program = Program(token, prog_name, block_node)
        self.eat(TokenType.DOT)
        program_node.parent(None)
        return program_node

    def block(self, **kwargs) -> Block:
        """
        block : declarations compound_statement
        """
        token: Token = self.current_token
        declaration_nodes: List[Union[Declarations]] = self.declarations(**kwargs)
        compound_statement_node: Compound = self.compound_statement(**kwargs)
        node: Block = Block(token, declaration_nodes, compound_statement_node)
        return node

    def declarations(self, **kwargs) -> List[Union[Declarations]]:
        """
        declarations : (VAR (variable_declaration SEMI)+)? procedure_declaration*
        """
        declarations: List[Union[Declarations]] = []

        if self.current_token.type == TokenType.VAR:
            self.eat(TokenType.VAR)
            while self.current_token.type == TokenType.ID:
                var_decl = self.variable_declaration(**kwargs)
                declarations.extend(var_decl)
                self.eat(TokenType.SEMI)

        while self.current_token.type == TokenType.PROCEDURE:
            proc_decl = self.procedure_declaration(**kwargs)
            declarations.append(proc_decl)

        return declarations

    def formal_parameters(self, **kwargs) -> List[Param]:
        """
        formal_parameters : ID (COMMA ID)* COLON type_spec
        """
        param_nodes: List[Param] = []

        param_tokens: List[Token] = [self.current_token]
        self.eat(TokenType.ID)
        while self.current_token.type == TokenType.COMMA:
            self.eat(TokenType.COMMA)
            param_tokens.append(self.current_token)
            self.eat(TokenType.ID)

        self.eat(TokenType.COLON)
        type_node: Type = self.type_spec(**kwargs)

        for param_token in param_tokens:
            param_node: Param = Param(param_token, Var(param_token), type_node)
            param_nodes.append(param_node)

        return param_nodes

    def formal_parameter_list(self, **kwargs) -> List[Param]:
        """
        formal_parameter_list : formal_parameters
                              | formal_parameters SEMI formal_parameter_list
        """
        # procedure Foo();
        if not self.current_token.type == TokenType.ID:
            return []

        param_nodes: List[Param] = self.formal_parameters(**kwargs)

        while self.current_token.type == TokenType.SEMI:
            self.eat(TokenType.SEMI)
            param_nodes.extend(self.formal_parameters(**kwargs))

        return param_nodes

    def variable_declaration(self, **kwargs) -> List[VarDecl]:
        """
        variable_declaration : ID (COMMA ID)* COLON type_spec
        """
        var_nodes = [(Var(self.current_token), self.current_token)]  # first ID
        self.eat(TokenType.ID)

        while self.current_token.type == TokenType.COMMA:
            self.eat(TokenType.COMMA)
            var_nodes.append((Var(self.current_token), self.current_token))
            self.eat(TokenType.ID)

        self.eat(TokenType.COLON)

        type_node = self.type_spec(**kwargs)
        var_declarations = [
            VarDecl(var_node[1], var_node[0], type_node)
            for var_node in var_nodes
        ]
        return var_declarations

    def procedure_declaration(self, **kwargs) -> ProcedureDecl:
        """
        procedure_declaration :
             PROCEDURE ID (LPAREN formal_parameter_list RPAREN)? SEMI block SEMI
        """
        token: Token = self.current_token
        self.eat(TokenType.PROCEDURE)
        proc_name: str = self.current_token.value
        self.eat(TokenType.ID)
        formal_params: List[AST] = []

        if self.current_token.type == TokenType.LPAREN:
            self.eat(TokenType.LPAREN)
            formal_params: List[Param] = self.formal_parameter_list(**kwargs)
            self.eat(TokenType.RPAREN)

        self.eat(TokenType.SEMI)
        block_node: Block = self.block(**kwargs)
        proc_decl: ProcedureDecl = ProcedureDecl(token, proc_name, formal_params, block_node)
        self.eat(TokenType.SEMI)
        return proc_decl

    def type_spec(self, **kwargs) -> Type:
        """
        type_spec : INTEGER
        """
        token: Token = self.current_token
        self.eat(TokenType.INTEGER)
        return Type(token)

    def compound_statement(self, **kwargs) -> Compound:
        """
        compound_statement: BEGIN statement_list END
        """
        token: Token = self.current_token
        self.eat(TokenType.BEGIN)
        nodes: List[AST] = self.statement_list(**kwargs)
        self.eat(TokenType.END)

        root: Compound = Compound(token)
        for node in nodes:
            root.children.append(node)

        return root

    def statement_list(self, **kwargs) -> List[Union[Statement]]:
        """
        statement_list : statement (SEMI statement)*
        """
        node: Union[Statement] = self.statement(**kwargs)

        results: List[Union[Statement]] = [node]

        while self.current_token.type == TokenType.SEMI:
            self.eat(TokenType.SEMI)
            results.append(self.statement(**kwargs))

        return results

    def statement(self, **kwargs) -> Union[Statement]:
        """
        statement : compound_statement
                  | proccall_statement
                  | assignment_statement
                  | if_statement
                  | builtin_call_statement
                  | while_loop
                  | repeat_loop
                  | break
                  | empty
        """
        if self.current_token.type == TokenType.BEGIN:
            node: Compound = self.compound_statement(**kwargs)
        elif (
                self.current_token.type == TokenType.ID and
                self.lexer.current_char == '('
        ):
            node: ProcedureCall = self.proccall_statement(**kwargs)
        elif self.current_token.type == TokenType.ID:
            node: Assign = self.assignment_statement(**kwargs)
        elif self.current_token.type == TokenType.IF:
            node: IfThenElse = self.if_statement(**kwargs)
        elif self.current_token.type in (TokenType.READLN, TokenType.WRITELN, TokenType.WRITEASCII):
            node: BuiltinFunction = self.builtin_call_statement(**kwargs)
        elif self.current_token.type == TokenType.WHILE:
            node: WhileLoop = self.while_loop(**kwargs)
        elif self.current_token.type == TokenType.REPEAT:
            node: RepeatLoop = self.repeat_loop(**kwargs)
        elif self.current_token.type == TokenType.BREAK:
            node: Break = self._break(**kwargs)
        else:
            node: NoOp = self.empty(**kwargs)
        return node

    def _break(self, **kwargs) -> Break:
        """
        break : BREAK
        """
        if not kwargs.get('loop'):
            self.error(ErrorCode.BREAK, self.current_token)
        node: Break = Break(self.current_token)
        self.eat(TokenType.BREAK)
        return node

    def proccall_statement(self, **kwargs) -> ProcedureCall:
        """
        proccall_statement : ID LPAREN (expr (COMMA expr)*)? RPAREN
        """
        token: Token = self.current_token

        proc_name: str = self.current_token.value
        self.eat(TokenType.ID)
        self.eat(TokenType.LPAREN)
        actual_params: List[AST] = []
        if self.current_token.type != TokenType.RPAREN:
            node: AST = self.expr(**kwargs)
            actual_params.append(node)

        while self.current_token.type == TokenType.COMMA:
            self.eat(TokenType.COMMA)
            node: AST = self.expr(**kwargs)
            actual_params.append(node)

        self.eat(TokenType.RPAREN)

        return ProcedureCall(
            proc_name=proc_name,
            actual_params=actual_params,
            token=token,
        )

    def builtin_call_statement(self, **kwargs) -> BuiltinFunction:
        """
        builtin_call_statement : WRITELN LPAREN expr COMMA expr RPAREN
                               | READLN LPAREN variable COMMA variable RPAREN
                               | WRITEASCII LPAREN expr COMMA expr RPAREN
        """
        token: Token = self.current_token
        self.eat(token.type)
        self.eat(TokenType.LPAREN)
        if token.type == TokenType.WRITELN:
            target: AST = self.expr(**kwargs)
            self.eat(TokenType.COMMA)
            content: AST = self.expr(**kwargs)
            self.eat(TokenType.RPAREN)
        elif token.type == TokenType.READLN:
            target: Var = self.variable(**kwargs)
            self.eat(TokenType.COMMA)
            content: Var = self.variable(**kwargs)
            self.eat(TokenType.RPAREN)
        else:
            target: AST = self.expr(**kwargs)
            self.eat(TokenType.COMMA)
            content: AST = self.expr(**kwargs)
            self.eat(TokenType.RPAREN)
        return BuiltinFunction(
            token=token,
            arguments=[target, content]
        )

    def while_loop(self, **kwargs) -> WhileLoop:
        """
        while_loop: WHILE expr DO statement
        """
        token: Token = self.current_token
        self.eat(TokenType.WHILE)
        expr: AST = self.expr(**kwargs)
        self.eat(TokenType.DO)
        statement: AST = self.statement(**{'loop': True, **kwargs})
        return WhileLoop(
            token=token,
            expr=expr,
            statement=statement
        )

    def repeat_loop(self, **kwargs) -> RepeatLoop:
        """
        repeat_loop: REPEAT statement UNTIL expr
        """
        token: Token = self.current_token
        self.eat(TokenType.REPEAT)
        statement: AST = self.statement(**{'loop': True, **kwargs})
        self.eat(TokenType.UNTIL)
        expr: AST = self.expr(**kwargs)
        return RepeatLoop(
            token=token,
            statement=statement,
            expr=expr
        )

    def assignment_statement(self, **kwargs) -> Assign:
        """
        assignment_statement : variable ASSIGN expr
        """
        left: Var = self.variable(**kwargs)
        token: Token = self.current_token
        self.eat(TokenType.ASSIGN)
        right: AST = self.expr(**kwargs)
        return Assign(left, token, right)

    def if_statement(self, **kwargs) -> IfThenElse:
        """
        if_statement : IF LPAREN expr RPAREN THEN statement (ELSE statement)?
        """
        token: Token = self.current_token
        self.eat(TokenType.IF)
        self.eat(TokenType.LPAREN)
        expr: AST = self.expr(**kwargs)
        self.eat(TokenType.RPAREN)
        self.eat(TokenType.THEN)
        then_statement: AST = self.statement(**kwargs)
        if self.current_token.type == TokenType.ELSE:
            self.eat(TokenType.ELSE)
            else_statement: AST = self.statement(**kwargs)
        else:
            else_statement: NoOp = self.empty(**kwargs)
        return IfThenElse(
            token=token,
            expr=expr,
            then_statement=then_statement,
            else_statement=else_statement
        )

    def variable(self, **kwargs) -> Var:
        """
        variable : ID
        """
        node: Var = Var(self.current_token)
        self.eat(TokenType.ID)
        return node

    def empty(self, **kwargs) -> NoOp:
        """
        empty :
        """
        return NoOp(self.current_token)

    def expr(self, **kwargs) -> Union[Expr]:
        """
        expr : simple_expr ((EQUAL | NOT_EQUAL | GREATER | LESS | GREATER_EQUAL | LESS_EQUAL) simple_expr)*
        """
        node: Union[Expr] = self.simple_expr(**kwargs)
        while self.current_token.type in (TokenType.EQUAL, TokenType.NOT_EQUAL, TokenType.GREATER, TokenType.LESS,
                                          TokenType.GREATER_EQUAL, TokenType.LESS_EQUAL):
            token: Token = self.current_token
            self.eat(self.current_token.type)
            node = BinOp(
                left=node,
                token=token,
                right=self.simple_expr(**kwargs)
            )
        return node

    def simple_expr(self, **kwargs) -> Union[Expr]:
        """
        simple_expr : term ((PLUS | MINUS | OR | XOR | OR_ALT_1 | OR_ALT_2) term)*
        """
        node: Union[Expr] = self.term(**kwargs)

        while self.current_token.type in (TokenType.PLUS, TokenType.MINUS, TokenType.OR, TokenType.XOR,
                                          TokenType.OR_ALT_1, TokenType.OR_ALT_2, TokenType.MODULO):
            token: Token = self.current_token
            if token.type in (TokenType.OR_ALT_1, TokenType.OR_ALT_2):
                token.type = TokenType.OR
            self.eat(self.current_token.type)

            node = BinOp(left=node, token=token, right=self.term(**kwargs))

        return node

    def term(self, **kwargs) -> Union[Expr]:
        """
        term : factor ((MUL
             | INTEGER_DIV
             | FLOAT_DIV
             | AND
             | AND_ALT
             | MOD
             | BIT_LEFT_SHIFT
             | BIT_RIGHT_SHIFT
             | BIT_RIGHT_SHIFT_ALT
             | BIT_LEFT_SHIFT_ALT) factor)*
        """
        node: Union[Expr] = self.factor(**kwargs)

        while self.current_token.type in (
                TokenType.MUL,
                TokenType.INTEGER_DIV,
                TokenType.FLOAT_DIV,
                TokenType.AND,
                TokenType.AND_ALT,
                TokenType.MOD,
                TokenType.BIT_LEFT_SHIFT,
                TokenType.BIT_RIGHT_SHIFT,
                TokenType.BIT_RIGHT_SHIFT_ALT,
                TokenType.BIT_LEFT_SHIFT_ALT,
        ):
            token = self.current_token
            if token.type == TokenType.BIT_RIGHT_SHIFT_ALT:
                token.type = TokenType.BIT_RIGHT_SHIFT
            elif token.type == TokenType.BIT_LEFT_SHIFT_ALT:
                token.type = TokenType.BIT_LEFT_SHIFT
            elif token.type == TokenType.AND_ALT:
                token.type = TokenType.AND
            self.eat(self.current_token.type)

            node: BinOp = BinOp(left=node, token=token, right=self.factor(**kwargs))

        return node

    def factor(self, **kwargs) -> Union[Expr]:
        """
        factor : PLUS factor
               | MINUS factor
               | NOT factor
               | BIT_NOT factor
               | exponent_factor
        """
        token: Token = self.current_token
        if token.type in (TokenType.PLUS, TokenType.MINUS, TokenType.NOT, TokenType.BIT_NOT):
            self.eat(token.type)
            return UnaryOp(token, self.factor(**kwargs))
        else:
            return self.exponent_factor(**kwargs)

    def exponent_factor(self, **kwargs) -> Union[Expr]:
        """
        exponent_factor : number ((POW | EXPONENT) number)*
        """
        node: Union[Expr] = self.number(**kwargs)

        while self.current_token.type in (TokenType.POW, TokenType.EXPONENT):
            token: Token = self.current_token
            if token.type == TokenType.EXPONENT:
                token.type = TokenType.POW
            self.eat(self.current_token.type)
            node = BinOp(
                left=node,
                token=token,
                right=self.number(**kwargs)
            )
        return node

    def number(self, **kwargs) -> Union[Expr]:
        """
        number: INTEGER_CONST
              | REAL_CONST
              | TRUE
              | FALSE
              | LPAREN expr RPAREN
              | variable
        """
        token: Token = self.current_token
        if token.type == TokenType.INTEGER_CONST:
            self.eat(TokenType.INTEGER_CONST)
            return Num(token)
        elif token.type == TokenType.LPAREN:
            self.eat(TokenType.LPAREN)
            node = self.expr(**kwargs)
            self.eat(TokenType.RPAREN)
            return node
        elif token.type in (TokenType.TRUE, TokenType.FALSE):
            self.eat(token.type)
            node = Bool(token)
            return node
        elif token.type == TokenType.ID:
            node = self.variable(**kwargs)
            return node
        else:
            self.error(ErrorCode.UNEXPECTED_TOKEN, token)

    def parse(self, **kwargs):
        """
        program : PROGRAM variable SEMI block DOT

        block : declarations compound_statement

        declarations : (VAR (variable_declaration SEMI)+)? procedure_declaration*

        formal_parameters : ID (COMMA ID)* COLON type_spec

        formal_parameter_list : formal_parameters
                              | formal_parameters SEMI formal_parameter_list

        variable_declaration : ID (COMMA ID)* COLON type_spec

        procedure_declaration :
             PROCEDURE ID (LPAREN formal_parameter_list RPAREN)? SEMI block SEMI

        type_spec : INTEGER

        compound_statement : BEGIN statement_list END

        statement_list : statement (SEMI statement)*

        statement : compound_statement
                  | proccall_statement
                  | assignment_statement
                  | if_statement
                  | builtin_call_statement
                  | while_loop
                  | repeat_loop
                  | break
                  | empty

        break : BREAK #Only possible within loops

        proccall_statement : ID LPAREN (expr (COMMA expr)*)? RPAREN

        builtin_call_statement : WRITELN LPAREN expr COMMA expr RPAREN
                               | READLN LPAREN variable COMMA variable RPAREN

        while_loop: WHILE expr DO statement

        repeat_loop: REPEAT statement UNTIL expr

        assignment_statement : variable ASSIGN expr

        if_statement : IF LPAREN expr RPAREN THEN statement (ELSE statement)?

        variable : ID

        empty :

        expr : simple_expr ((EQUAL | NOT_EQUAL | GREATER | LESS | GREATER_EQUAL | LESS_EQUAL) simple_expr)*

        simple_expr : term ((PLUS | MINUS | OR | XOR | OR_ALT_1 | OR_ALT_2) term)*

        term : factor ((MUL
             | INTEGER_DIV
             | FLOAT_DIV
             | AND
             | AND_ALT
             | MOD
             | BIT_LEFT_SHIFT
             | BIT_RIGHT_SHIFT
             | BIT_RIGHT_SHIFT_ALT
             | BIT_LEFT_SHIFT_ALT) factor)*

        factor : PLUS factor
               | MINUS factor
               | NOT factor
               | BIT_NOT factor
               | exponent_factor

        exponent_factor : number ((POW | EXPONENT) number)*

        number: INTEGER_CONST
              | REAL_CONST
              | TRUE
              | FALSE
              | LPAREN expr RPAREN
              | variable
        """
        node = self.program(**kwargs)
        if self.current_token.type != TokenType.EOF:
            self.error(
                error_code=ErrorCode.UNEXPECTED_TOKEN,
                token=self.current_token,
            )

        return node
