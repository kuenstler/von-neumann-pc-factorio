"""BlockStack for GCSE and LCSE for SPI"""

from __future__ import annotations

from abc import abstractmethod, ABC
from typing import TypedDict, Tuple, TypeVar

from Compiler.ASTNodes import *
from Compiler.GenericASTVisitors import GenericVisitorMethods, NodeVisitor
from Compiler.Utilities import generic_str

count = 0


class Variables(TypedDict, total=False):
    type: str
    var: List[ContextVar]
    node: Assign


class DependsOn(GenericVisitorMethods, NodeVisitor):
    def visit_Var(self, node: Var, **kwargs):
        return [node]

    def visit_ContextVar(self, node: ContextVar, **kwargs):
        return [node]

    def visit_BinOp(self, node: BinOp, **kwargs):
        return self.visit(node.left, **kwargs) + self.visit(node.right, **kwargs)

    def visit_UnaryOp(self, node: UnaryOp, **kwargs):
        return self.visit(node.expr, **kwargs)

    def visit_Num(self, node: Num, **kwargs):
        return []

    def visit_Bool(self, node: Bool, **kwargs):
        return []


depends_on = DependsOn()


class IAddVarDecl:
    @abstractmethod
    def add_var_decl(self, var: Var, var_type: Type):
        raise NotImplementedError(f'add_var_decl not found in {self.__class__.__name__}')


class IGetCOMPVar(IAddVarDecl, ABC):
    def get_comp_var(self, node: AST) -> Tuple[Assign, Var]:
        global count
        var = Var(node.token, f'_COMP{count}')
        count += 1
        var_type: Type = Type(node.token, TokenType.INTEGER.value)
        self.add_var_decl(var, var_type)
        assign = Assign(var, node.token, node)
        return assign, var


class IComputable:
    @abstractmethod
    def compute(self):
        raise NotImplementedError(f'compute not found in {self.__class__.__name__}')


class ISearchable:
    @abstractmethod
    def search_for_node_write(self, node: Expr, context: ISearchable, look_at_parent: bool = True) -> List[Variables]:
        raise NotImplementedError(f'search_for_node_write not found in {self.__class__.__name__}')

    @abstractmethod
    def search_for_var_change(self, start_pos: int, dependent_variables: List[ContextVar],
                              context: ISearchable, iterations: int = 0) -> List[List[int]]:
        raise NotImplementedError(f'search_for_var_change not found in {self.__class__.__name__}')

    @abstractmethod
    def search_for_COMP_assign(self, var: ContextVar, context: ISearchable) -> Optional[Tuple[int, Variables]]:
        raise NotImplementedError(f'search_for_COMP_assign not found in {self.__class__.__name__}')


class IPushable:
    @abstractmethod
    def push_forward(self, method: List[Tree], context: IPushable):
        raise NotImplementedError(f'push_forward not found in {self.__class__.__name__}')

    @abstractmethod
    def push_backward(self, method: List[Tree], context: IPushable):
        raise NotImplementedError(f'push_backward not found in {self.__class__.__name__}')

    @abstractmethod
    def push_forward_to_end(self, method: List[Tree], context: IPushable):
        raise NotImplementedError(f'push_forward_to_end not found in {self.__class__.__name__}')


class IStatements:
    @property
    @abstractmethod
    def statements(self) -> List[Union[Statement, BranchingBlockStack]]:
        raise NotImplementedError(f'statements not found in {self.__class__.__name__}')


class IFinalTree:
    @property
    @abstractmethod
    def final_tree(self) -> AST:
        raise NotImplementedError(f'tree not found in {self.__class__.__name__}')


class IBasicInteraction(IComputable, ISearchable, IPushable, IStatements, IFinalTree, IAddVarDecl, ABC):
    pass


class BasicBlock(GenericVisitorMethods, NodeVisitor, IBasicInteraction, IGetCOMPVar):
    def __init__(self, statements: List[Union[Statement]], parent: BasicBlockStack, variables: List[Variables] = None):
        self.init_variables: List[Variables] = variables or []
        self.variables: List[Variables] = self.init_variables
        self._statements: List[Union[Statement]] = statements
        self.parent: BasicBlockStack = parent
        self.index = 0
        super().__init__()

    @property
    def statements(self) -> List[Union[Statement, BranchingBlockStack]]:
        return self._statements

    @statements.setter
    def statements(self, value: List[Union[Statement, BranchingBlockStack]]):
        self._statements = value

    @property
    def final_tree(self) -> List[AST]:
        return self.statements

    def walker(self):
        for i in self.statements:
            self.index = self.statements.index(i)
            self.visit(i)

    def compute(self):
        self.walker()
        self.variables: List[Variables] = self.init_variables
        self.walker()

    def __len__(self):
        return self.statements

    def add_var_decl(self, var: Var, var_type: Type):
        self.parent.add_var_decl(var, var_type)

    def visit_Assign(self, node: Assign, **kwargs):
        self.visit(node.right, **kwargs)
        temp_list: List[Variables] = [i for i in self.variables if i['type'] == 'write'
                                      and i['node'].right == node.right]
        if any(i for i in temp_list if i['node'] == node):
            return
        #        if any(temp_list):
        #            node.right = temp_list[-1]['var']
        else:
            dependencies: List[ContextVar] = [ContextVar(i, self) if isinstance(i, Var) else i
                                              for i in [node.left] + (depends_on.visit(node.right) or [])]
            self.variables.append({'type': 'write', 'node': node, 'var': dependencies})

    def visit_Var(self, node: Var, **kwargs):
        self.variables.append({'type': 'read', 'var': [ContextVar(node, self)]})

    def visit_ContextVar(self, node: ContextVar, **kwargs):
        self.variables.append({'type': 'read', 'var': [node]})

    def __str__(self):
        return generic_str(self, ['parent'])

    def __repr__(self):
        return self.__str__()

    def search_for_node_write(self, node: Expr, context: ISearchable, look_at_parent: bool = True) -> List[Variables]:
        """Search if the given Expr has been done before by a compiler optimisation"""
        own_list: List[Variables] = [i for i in self.variables if (
                i['type'] == 'write'
                and i['node'].right == node
                and i['var'][0].value.startswith('_COMP'))]
        if not own_list and look_at_parent:
            return self.parent.search_for_node_write(node, self)
        return own_list

    def search_for_COMP_assign(self, var: ContextVar, context: ISearchable) -> Optional[Tuple[int, Variables]]:
        """Search for a Compiler Assign using a given Var, returns None if Var is not a Compiler Variable"""
        if not var.value.startswith('_COMP'):
            return None
        own_list: List[Tuple[int, Variables]] = [
            (j, self.variables[j]) for j in range(len(self.variables))
            if (self.variables[j]['type'] == 'write' and self.variables[j]['var'][0] == var)
        ]
        if own_list:
            return own_list[-1]
        else:
            return self.parent.search_for_COMP_assign(var, self)

    def search_for_var_change(self, start_pos: int, dependent_variables: List[ContextVar],
                              context: ISearchable, iterations: int = 0) -> List[List[int]]:
        """Find all occurrences of dependent variables after start_pos and till stop_pos"""
        if start_pos < len(self.variables):
            own: List[bool] = [i.context is self for i in dependent_variables]
            before: List[List[int]] = [[] if i else list(range(start_pos)) for i in own]
            between: List[List[int]] = [list(range(start_pos, len(self.variables))) for i in dependent_variables]
            merged: List[List[int]] = [before[i] + between[i] for i in range(len(before))]
        else:
            merged: List[List[int]] = [list(range(len(self.variables))) for i in dependent_variables]
        dependencies: List[List[int]] = [[row for row in merged[dependency] if (
                self.variables[row]['type'] == 'write'
                and self.variables[row]['var'][0] == dependent_variables[dependency]
        )] for dependency in range(len(merged))]
        other_dependencies: List[List[int]] = self.parent.search_for_var_change(start_pos,
                                                                                dependent_variables, self,
                                                                                iterations + 1)
        return dependencies + other_dependencies

    def check_node(self, node: Expr):
        if isinstance(node, Endpoints):
            # False means there is nothing to do
            return False
        in_list: List[Variables] = self.search_for_node_write(node, self)
        if in_list:
            last = in_list[-1]  # get only the last occurrence of the corresponding Assign
            # initialise the list of variables to check, tell python to don't use a reference
            vars: List[ContextVar] = last['var'].copy()
            # initialise the list of commands to be run with the corresponding Assign
            commands: List[Variables] = [last]
            for var in vars:
                # iterate over all vars till none are left
                # ignore all vars that don't start with _COMP (the reserved vars for the compiler)
                if var.value.startswith('_COMP'):
                    # get a potential command and it's index to re calculate the needed variable, use only the last
                    # occurrence
                    command: Tuple[int, Variables] = self.search_for_COMP_assign(var, self)
                    # check if any variable the command is based on will be overwritten afterwards
                    if any(self.search_for_var_change(command[0], command[1]['var'][1:], self)):
                        # if so, write the command in front of all other commands
                        commands.insert(0, command[1])
            return commands
        # never means there is currently no variable that covers this expression
        return 'never'

    def treat_node(self, node: Expr) -> Expr:
        what_to_do = self.check_node(node)
        if what_to_do is False:
            return node
        if what_to_do == 'never':
            assign, var = self.get_comp_var(node)
            self.statements.insert(self.index, assign)
            self.index += 1
            self.visit(assign)
            return var
        if isinstance(what_to_do, list):
            var: Var = what_to_do[-1]['var'][0].var
            for i in what_to_do[:-1]:
                self.statements.insert(self.index, i['node'])
                self.index += 1
            return var
        raise NotImplementedError(f'Something went horribly wrong at node\n{node}\nat treat_node')

    def visit_BinOp(self, node: BinOp, **kwargs):
        self.visit(node.left, **kwargs)
        node.left = self.treat_node(node.left)
        self.visit(node.right, **kwargs)
        node.right = self.treat_node(node.right)

    def visit_UnaryOp(self, node: UnaryOp, **kwargs):
        self.visit(node.expr, **kwargs)
        node.expr = self.treat_node(node.expr)

    def visit_Num(self, node: Num, **kwargs):
        pass

    def visit_Bool(self, node: Bool, **kwargs):
        pass

    def visit_BuiltinFunction(self, node: BuiltinFunction, **kwargs):
        for i in range(len(node.arguments)):
            self.visit(node.arguments[i], **kwargs)
            node.arguments[i] = self.treat_node(node.arguments[i])

    def visit_NoOp(self, node: NoOp, **kwargs):
        pass

    def push_forward(self, method: List[Tree], context: IPushable):
        self.statements[:0] = method

    def push_backward(self, method: List[Tree], context: IPushable):
        self.statements.extend(method)

    def push_forward_to_end(self, method: List[Tree], context: IPushable):
        self.statements.extend(method)


BasicBlockOrStructure = (Tree, BasicBlock)


class BasicBlockStack(IBasicInteraction, ABC):
    def __init__(self, tree: Tree, parent_stack: Optional[BasicBlockStack]):
        self.tree: Tree = tree
        self.parent_stack: Optional[BasicBlockStack] = parent_stack
        super().__init__()

    def __str__(self):
        return generic_str(self, ['parent_stack', 'parent_tree'])

    def __repr__(self):
        return str(self)

    def add_var_decl(self, var: Var, var_type: Type):
        self.parent_stack.add_var_decl(var, var_type)

    @property
    def statements(self) -> List[Union[Statement, BranchingBlockStack]]:
        raise AttributeError(f'property statements not expected in {self.__class__.__name__}')


class ProgramStackProxy(BasicBlockStack):
    def __init__(self, tree: Program):
        super().__init__(tree, None)
        self.compound: CompoundBlockStack = CompoundBlockStack(self.tree.block.compound_statement, self)

    def compute(self):
        self.compound.compute()

    @property
    def final_tree(self) -> Program:
        self.tree.block.compound_statement = self.compound.final_tree
        return self.tree

    def add_var_decl(self, var: Var, var_type: Type):
        self.tree.block.declarations.append(VarDecl(var.token, var, var_type))

    def push_backward(self, method: List[Tree], context: IPushable):
        raise RuntimeError('Should not push back to Program')

    def push_forward_to_end(self, method: List[Tree], context: IPushable):
        raise RuntimeError('Should not push forward to end to Program')

    def push_forward(self, method: List[Tree], context: IPushable):
        raise RuntimeError('Should not push forward to Program')

    def search_for_var_change(self, start_pos: int, dependent_variables: List[ContextVar],
                              context: ISearchable, iterations: int = 0) -> List[List[int]]:
        return []

    def search_for_node_write(self, node: Expr, context: ISearchable, look_at_parent: bool = True) -> List[Variables]:
        return []

    def search_for_COMP_assign(self, var: ContextVar, context: ISearchable) -> Optional[Tuple[int, Variables]]:
        return None


class BlockStackThread(BasicBlockStack):
    def __init__(self, tree: Tree, parent_stack: Optional[BasicBlockStack],
                 basic_blocks: List[BasicBlockOrStructure] = None):
        super().__init__(tree, parent_stack)
        self.basic_blocks: List[BasicBlockOrStructure] = basic_blocks or []

    @property
    def final_tree(self) -> AST:
        if isinstance(self.tree, Compound):
            children = []
            for i in self.basic_blocks:
                ast = i.final_tree
                if not isinstance(ast, list):
                    ast = [ast]
                children.extend(ast)
            self.tree.children = children
            return self.tree
        if isinstance(self.tree, IFinalTree):
            return self.tree.final_tree
        return self.tree

    def check_compound(self):
        if not isinstance(self.tree, Compound) and len(self.basic_blocks[0].statements) > 1:
            self.tree = Compound(self.basic_blocks[0].statements[-1].token, children=self.basic_blocks[0].statements)

    @classmethod
    def cleanup_compound(cls, tree: Compound):
        """Clean up nested Compound Statements"""
        while any(isinstance(i, Compound) for i in tree.children):
            for i in tree.children:
                if isinstance(i, Compound):
                    index = tree.children.index(i)
                    tree.children.remove(i)
                    tree.children = tree.children[:index] + i.children + tree.children[index:]

    def treat_compound(self, tree: Compound):
        """get basic blocks from Compound"""
        self.cleanup_compound(tree)
        statements = []
        for i in tree.children:
            if not isinstance(i, BranchingNodes):
                statements.append(i)
            else:
                if statements:
                    self.basic_blocks.append(BasicBlock(statements, self))
                self.treat_branching_method(i)
                statements = []
        if statements:
            self.basic_blocks.append(BasicBlock(statements, self))
        tree.children.clear()
        for i in self.basic_blocks:
            if isinstance(i, BasicBlock):
                tree.children += i.statements

    def treat_single_statement(self, tree: Tree):
        """Get basic block from single statement, returns compound if statement leads to several statements"""
        self.basic_blocks = [BasicBlock([tree], self)]
        self.check_compound()

    def dismantle(self, tree: Tree):
        """Dismantle a Compound, single statement or branching node"""
        if isinstance(tree, Compound):
            self.treat_compound(tree)
        # single statements
        elif not isinstance(tree, BranchingNodes):
            self.treat_single_statement(tree)
        else:
            self.treat_branching_method(tree)

    def search_for_node_write(self, node: Expr, context: ISearchable, look_at_parent: bool = True) -> List[Variables]:
        if context is self.parent_stack:
            index = len(self.basic_blocks)
        else:
            index = self.basic_blocks.index(context)
        if index == 0:
            if not self.parent_stack or not look_at_parent:
                return []
            return self.parent_stack.search_for_node_write(node, self)
        return self.basic_blocks[index - 1].search_for_node_write(node, self, look_at_parent=look_at_parent)

    def search_for_COMP_assign(self, var: ContextVar, context: ISearchable) -> Optional[Tuple[int, Variables]]:
        if context is self.parent_stack:
            index = len(self.basic_blocks)
        else:
            index = self.basic_blocks.index(context)
        if index == 0:
            if not self.parent_stack:
                return None
            return self.parent_stack.search_for_COMP_assign(var, self)
        return self.basic_blocks[index - 1].search_for_COMP_assign(var, self)

    def search_for_var_change(self, start_pos: int, dependent_variables: List[ContextVar],
                              context: ISearchable, iterations: int = 0) -> List[List[int]]:
        iterations += 1
        if context is self.parent_stack:
            index = len(self.basic_blocks)
        else:
            index = self.basic_blocks.index(context)
        if index == 0:
            if not self.parent_stack:
                return []
            return self.parent_stack.search_for_var_change(start_pos, dependent_variables, self, iterations)
        return self.basic_blocks[index - 1].search_for_var_change(
            start_pos, dependent_variables, self, iterations)

    def push_forward(self, method: List[Tree], context: IPushable):
        if context is self.parent_stack:
            self.basic_blocks[0].push_forward(method, self)
        else:
            index = self.basic_blocks.index(context)
            if index == len(self.basic_blocks) - 1:
                if self.parent_stack:
                    self.parent_stack.push_forward(method, self)
            else:
                self.basic_blocks[index + 1].push_forward(method, self)
        self.check_compound()

    def push_forward_to_end(self, method: List[Tree], context: IPushable):
        if context is self.parent_stack:
            self.basic_blocks[-1].push_forward_to_end(method, self)
        else:
            index = self.basic_blocks.index(context)
            if index == len(self.basic_blocks) - 1:
                if self.parent_stack:
                    self.parent_stack.push_forward_to_end(method, self)
            else:
                self.basic_blocks[-1].push_forward_to_end(method, self)
        self.check_compound()

    def push_backward(self, method: List[Tree], context: IPushable):
        if context is self.parent_stack:
            self.basic_blocks[-1].push_backward(method, self)
        else:
            index = self.basic_blocks.index(context)
            if index == 0:
                if self.parent_stack:
                    self.parent_stack.push_backward(method, self)
            else:
                self.basic_blocks[index - 1].push_backward(method, self)
        self.check_compound()

    def treat_branching_method(self, method: BranchingNodes):
        if isinstance(method, IfThenElse):
            self.basic_blocks.append(IfThenElseBlockStack(method, self))
        elif isinstance(method, WhileLoop):
            self.basic_blocks.append(WhileBlockStack(method, self))
        elif isinstance(method, RepeatLoop):
            self.basic_blocks.append(RepeatBlockStack(method, self))
        elif isinstance(method, ProcedureCall):
            raise NotImplementedError('Tried to atomarise Procedure Call, but it is not implemented')
        else:
            raise NotImplementedError(f'Shouldn\'t reach this, but {method} did it.')

    def compute(self):
        for i in self.basic_blocks:
            i.compute()


class CompoundBlockStack(BlockStackThread):
    def __init__(self, tree: Tree, parent_stack: Optional[BasicBlockStack],
                 basic_blocks: List[BasicBlockOrStructure] = None):
        """single statement or Compound statement, no limitations"""
        super().__init__(tree, parent_stack, basic_blocks)

    def compute(self):
        self.dismantle(self.tree)
        super().compute()


class EndpointBlockStack(CompoundBlockStack, IGetCOMPVar):
    def __init__(self, tree: Expr, parent_stack: Optional[BasicBlockStack],
                 basic_blocks: List[BasicBlockOrStructure] = None, afterwards: bool = False):
        """single statement or Compound statement, limited to be a var in the long run"""
        super().__init__(tree, parent_stack, basic_blocks)
        self.afterwards: bool = afterwards
        self.basic_block: Optional[BasicBlockOrStructure] = None
        self.statement: Optional[List[Statement]] = None
        self.endpoint: Optional[Endpoints] = None

    @property
    def final_tree(self) -> AST:
        return self.endpoint

    def compute(self):
        super().compute()
        self.basic_block: BasicBlockOrStructure = self.basic_blocks[0]
        self.statement: List[Statement] = self.basic_block.statements
        self.endpoint: Endpoints = self.statement[-1]
        if not isinstance(self.endpoint, (Assign, Endpoints)):
            self.endpoint, _ = self.get_comp_var(self.endpoint)
            self.basic_blocks[0].statements[-1] = self.endpoint
        if not isinstance(self.endpoint, Endpoints) or len(self.statement) > 1:
            if self.afterwards:
                self.parent_stack.push_forward_to_end(self.statement, self)
            self.parent_stack.push_backward(self.statement, self)
            self.endpoint = self.statement[-1].left


class BranchingBlockStack(BasicBlockStack, ABC):
    pass


class IfThenElseBlockStack(BranchingBlockStack):
    def __init__(self, tree: IfThenElse, parent_stack: Optional[BasicBlockStack]):
        super().__init__(tree, parent_stack)
        self.expr: EndpointBlockStack = EndpointBlockStack(self.tree.expr, self)
        self.then_statement: CompoundBlockStack = CompoundBlockStack(self.tree.then_statement, self)
        self.else_statement: CompoundBlockStack = CompoundBlockStack(self.tree.else_statement, self)

    def compute(self):
        self.expr.compute()
        self.then_statement.compute()
        self.else_statement.compute()

    @property
    def final_tree(self) -> IfThenElse:
        self.tree.expr = self.expr.final_tree
        self.tree.then_statement = self.then_statement.final_tree
        self.tree.else_statement = self.else_statement.final_tree
        return self.tree

    def push_forward(self, method: List[Tree], context: IPushable):
        if context is self.expr:
            self.then_statement.push_forward(method, self)
        elif context is self.then_statement or context is self.else_statement:
            self.parent_stack.push_forward(method, self)
        else:
            self.then_statement.push_forward(method, self)
            self.else_statement.push_forward(method, self)

    def push_forward_to_end(self, method: List[Tree], context: IPushable):
        raise AttributeError(
            f'push_forward_to_end does not work with IfThenElseBlockStack\n{method}'
        )  # lazy move, shouldn't happen

    def push_backward(self, method: List[Tree], context: IPushable):
        if context is self.expr or context is self.then_statement or context is self.else_statement:
            self.parent_stack.push_backward(method, self)
        else:
            self.then_statement.push_backward(method, self)
            self.else_statement.push_backward(method, self)

    def search_for_node_write(self, node: Expr, context: ISearchable, look_at_parent: bool = True) -> List[Variables]:
        if context is self.expr or context is self.then_statement or context is self.else_statement:
            if look_at_parent:
                return self.parent_stack.search_for_node_write(node, self)
            return []
        own_list: List[Variables] = self.then_statement.search_for_node_write(node, self, look_at_parent=False)
        own_list += self.else_statement.search_for_node_write(node, self, look_at_parent=False)
        if look_at_parent:
            own_list += self.parent_stack.search_for_node_write(node, self)
        return own_list

    def search_for_var_change(self, start_pos: int, dependent_variables: List[ContextVar],
                              context: ISearchable, iterations: int = 0) -> List[List[int]]:
        if context is self.expr or context is self.then_statement or context is self.else_statement:
            return self.parent_stack.search_for_var_change(start_pos, dependent_variables, self, iterations)
        own_list: List[List[int]] = self.then_statement.search_for_var_change(
            start_pos, dependent_variables, self, iterations)
        own_list += self.else_statement.search_for_var_change(
            start_pos, dependent_variables, self, iterations)
        own_list += self.parent_stack.search_for_var_change(start_pos, dependent_variables, self, iterations)
        return own_list

    def search_for_COMP_assign(self, var: ContextVar, context: ISearchable) -> Optional[Tuple[int, Variables]]:
        if context is self.expr or context is self.then_statement or context is self.else_statement:
            return self.parent_stack.search_for_COMP_assign(var, self)
        own_list: Optional[Tuple[int, Variables]] = self.then_statement.search_for_COMP_assign(var, self)
        own_list = own_list or self.else_statement.search_for_COMP_assign(var, self)
        own_list = own_list or self.parent_stack.search_for_COMP_assign(var, self)
        return own_list


class LoopBlockStack(BranchingBlockStack):
    _T = TypeVar('_T', WhileLoop, RepeatLoop)

    def __init__(self, tree: _T, parent_stack: Optional[BasicBlockStack]):
        super().__init__(tree, parent_stack)
        self.expr: EndpointBlockStack = EndpointBlockStack(self.tree.expr, self, afterwards=True)
        self.statement: CompoundBlockStack = CompoundBlockStack(self.tree.statement, self)

    def compute(self):
        self.statement.compute()
        self.expr.compute()

    @property
    def final_tree(self) -> _T:
        self.tree.expr = self.expr.final_tree
        self.tree.statement = self.statement.final_tree
        return self.tree

    def push_forward(self, method: List[Tree], context: IPushable):
        if context is self.expr:
            raise RuntimeError('expr of WhileLoop is not intended to push forward')
        elif context is self.statement:
            self.parent_stack.push_forward(method, self)
        elif context is self.parent_stack:
            self.statement.push_forward(method, self)
        else:
            raise RuntimeError(f'Unexpected context in WhileBlockStack\n{context}')

    def push_forward_to_end(self, method: List[Tree], context: IPushable):
        if context is self.expr or context is self.parent_stack:
            self.statement.push_forward_to_end(method, self)
        elif context is self.statement:
            self.parent_stack.push_forward_to_end(method, self)
        else:
            raise RuntimeError(f'Unexpected context in WhileBlockStack\n{context}')

    def push_backward(self, method: List[Tree], context: IPushable):
        if context is self.expr or context is self.statement:
            self.parent_stack.push_backward(method, self)
        elif context is self.parent_stack:
            self.statement.push_backward(method, self)
        else:
            raise RuntimeError(f'Unexpected context in WhileBlockStack\n{context}')

    def search_for_COMP_assign(self, var: ContextVar, context: ISearchable) -> Optional[Tuple[int, Variables]]:
        if context is self.expr or context is self.statement:
            return self.parent_stack.search_for_COMP_assign(var, self)
        elif context is self.parent_stack:
            return self.statement.search_for_COMP_assign(var, self)
        else:
            raise RuntimeError(f'Unexpected context in WhileBlockStack\n{context}')

    def search_for_node_write(self, node: Expr, context: ISearchable, look_at_parent: bool = True) -> List[Variables]:
        if context is self.expr or context is self.statement:
            if look_at_parent:
                return self.parent_stack.search_for_node_write(node, self)
            return []
        elif context is self.parent_stack:
            return self.statement.search_for_node_write(node, self)
        else:
            raise RuntimeError(f'Unexpected context in WhileBlockStack\n{context}')

    def search_for_var_change(self, start_pos: int, dependent_variables: List[ContextVar],
                              context: ISearchable, iterations: int = 0) -> List[List[int]]:
        if context is self.expr or context is self.statement:
            return self.parent_stack.search_for_var_change(start_pos, dependent_variables, self, iterations)
        elif context is self.parent_stack:
            return self.statement.search_for_var_change(start_pos, dependent_variables, self, iterations)
        else:
            raise RuntimeError(f'Unexpected context in WhileBlockStack\n{context}')


class RepeatBlockStack(LoopBlockStack):
    def __init__(self, tree: RepeatLoop, parent_stack: Optional[BasicBlockStack]):
        super().__init__(tree, parent_stack)


class WhileBlockStack(LoopBlockStack):
    def __init__(self, tree: WhileLoop, parent_stack: Optional[BasicBlockStack]):
        super().__init__(tree, parent_stack)
