"""AST Nodes for SPi"""

from __future__ import annotations

from typing import Optional, List, Union

from Compiler.Tokens import Token, TokenType
from Compiler.Utilities import generic_str


class AST:
    def __init__(self, token: Token, name: str):
        self.name: str = name
        self.token: Token = token
        self.parent_class: Optional[AST] = None

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        arguments = []
        for i in self.__dir__():
            if '__' not in i and i not in ['replace', 'parent_class', 'parent', 'var']:
                arguments.append(self.__getattribute__(i) == other.__getattribute__(i))
        return all(arguments)

    def __str__(self):
        return generic_str(self, ['replace', 'parent', 'parent_class'])

    def __dir(self):
        dir = []
        for i in self.__dir__():
            if not i.startswith('_') and i not in ['replace', 'parent', 'parent_class', 'var']:
                dir.append(i)
        return dir

    def __iter__(self):
        yield self
        for i in self.__dir():
            yield self.__getattribute__(i)

    def __repr__(self):
        return self.__str__()

    def parent(self, parent):
        self.parent_class = parent
        for i in self.__dir():
            node = self.__getattribute__(i)
            if isinstance(node, AST):
                node.parent(self)


class Num(AST):
    def __init__(self, token: Token, value: int = None):
        super().__init__(token, f'Number: {value}')
        # 0 is considered false and thus won't work the easy way
        self.value = value if value is not None else token.value


class Bool(AST):
    def __init__(self, token: Token, value: [bool, int] = None):
        super().__init__(token, f'Bool: {value}')
        if value:
            self.value = value
        elif token.type == TokenType.TRUE:
            self.value = 1
        else:
            self.value = 0


class Var(AST):
    """The Var node is constructed out of ID token."""

    def __init__(self, token, value: str = None):
        self._value: str = value or token.value
        super().__init__(token, f'Var: {self.value}')

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value: str):
        self._value = value
        self.token.value = value

    @property
    def var(self) -> Var:
        return self

    def __eq__(self, other):
        if isinstance(other, ContextVar):
            return super().__eq__(other.var)
        return super().__eq__(other)

    def __hash__(self):
        return hash((self.token, self.value))


class ContextVar(Var):
    def __init__(self, var: Var, context: BasicBlock):
        super().__init__(var.token, var.value)
        self.context: BasicBlock = context

    @property
    def var(self) -> Var:
        return Var(self.token, self.value)

    def __str__(self):
        return generic_str(self, ['context'])

    def __eq__(self, other):
        if isinstance(other, Var):
            return self.var == other
        return super().__eq__(other)


class BinOp(AST):
    def __init__(self, left: Union[Expr], token: Token, right: Union[Expr]):
        super().__init__(token, f'BinOp; left: {left.name}, right: {right.name}')
        self.left: AST = left
        self.right: AST = right

    def __eq__(self, other):
        if not isinstance(other, BinOp):
            return False
        if (
                self.token == other.token
                and self.left == other.left
                and self.right == other.right
        ):
            return True
        if self.token.type in (
                TokenType.PLUS,
                TokenType.MUL,
                TokenType.EQUAL,
                TokenType.AND,
                TokenType.OR,
                TokenType.XOR,
                TokenType.NOT_EQUAL
        ):
            return (
                    self.token == other.token
                    and self.left == other.right
                    and self.right == other.left
            )
        if self.token.type == TokenType.GREATER:
            return (
                    other.token.type == TokenType.LESS
                    and self.left == other.right
                    and self.right == other.left
            )
        if self.token.type == TokenType.LESS:
            return (
                    other.token.type == TokenType.GREATER
                    and self.left == other.right
                    and self.right == other.left
            )
        if self.token.type == TokenType.GREATER_EQUAL:
            return (
                    other.token.type == TokenType.LESS_EQUAL
                    and self.left == other.right
                    and self.right == other.left
            )
        if self.token.type == TokenType.LESS_EQUAL:
            return (
                    other.token.type == TokenType.GREATER_EQUAL
                    and self.left == other.right
                    and self.right == other.left
            )
        return False


class UnaryOp(AST):
    def __init__(self, token: Token, expr: Union[Expr]):
        super().__init__(token, f'UnaryOp; token: {token}, expr: {expr}')
        self.expr: AST = expr


Expr = (Num, Bool, Var, BinOp, UnaryOp)


class IfThenElse(AST):
    def __init__(self, token: Token, expr: Union[Expr], then_statement: Union[Statement],
                 else_statement: Union[Statement]):
        super().__init__(token, f'IfThenElse: {token}')
        self.expr: Union[Expr] = expr
        self.then_statement: Union[Statement] = then_statement
        self.else_statement: Union[Statement] = else_statement


class Break(AST):
    pass


class Assign(AST):
    def __init__(self, left: Var, token: Token, right: Union[Expr]):
        super().__init__(token, f'Assign: {left.name}')
        self.left: Var = left
        self.right: Union[Expr] = right


class RepeatLoop(AST):
    def __init__(self, token: Token, statement: Union[Statement], expr: Union[Expr]):
        super().__init__(token, f'RepeatLoop: {token}')
        self.statement: Union[Statement] = statement
        self.expr: Union[Expr] = expr


class WhileLoop(AST):
    def __init__(self, token: Token, expr: Union[Expr], statement: Union[Statement]):
        super().__init__(token, f'WhileLoop: {token}')
        self.expr: Union[Expr] = expr
        self.statement: Union[Statement] = statement


class BuiltinFunction(AST):
    def __init__(self, token: Token, arguments: List[Union[Expr]]):
        super().__init__(token, f'BuiltinFunction: {token}')
        self.arguments: List[Union[Expr]] = arguments


class ProcedureCall(AST):
    def __init__(self, proc_name: str, actual_params: List[Union[Expr]], token: Token, proc_symbol=None):
        super().__init__(token, f'ProcedureCall: {token}')
        self.proc_name: str = proc_name
        self.actual_params: List[Union[Expr]] = actual_params  # a list of AST nodes
        # a reference to procedure declaration symbol
        self.proc_symbol = proc_symbol  # of type ScopedSymbolTable


class Compound(AST):
    """Represents a 'BEGIN ... END' block"""

    def __init__(self, token: Token, children: List[Union[Statement]] = None):
        super().__init__(token, f'Compound: {token}')
        self.children: List[Union[Statement]] = children or []


class NoOp(AST):
    def __init__(self, token:Token):
        super().__init__(token, f'NoOp: {token}')


Statement = (Compound, ProcedureCall, Assign, IfThenElse, BuiltinFunction, WhileLoop, RepeatLoop, NoOp, Break)


class Block(AST):
    def __init__(self, token: Token, declarations: List[Union[Declarations]], compound_statement: Compound):
        super().__init__(token, f'Block: {token}')
        self.declarations: List[Union[Declarations]] = declarations
        self.compound_statement: Compound = compound_statement


class Type(AST):
    def __init__(self, token: Token, value=None):
        self.value = value or token.value
        super().__init__(token, f'Type: {self.value}')


class VarDecl(AST):
    def __init__(self, token: Token, var_node: Var, type_node: Type):
        super().__init__(token, f'VarDecl; var: {var_node.name}, type: {type_node.name}')
        self.var_node: Var = var_node
        self.type_node: Type = type_node


class Param(AST):
    def __init__(self, token: Token, var_node: Var, type_node: Type):
        super().__init__(token, f'Param; var: {var_node.name}, type: {type_node.name}')
        self.var_node: Var = var_node
        self.type_node: Type = type_node


class ProcedureDecl(AST):
    def __init__(self, token: Token, proc_name: str, formal_params: List[Param], block_node: Block):
        super().__init__(token, f'ProcedureDecl: {proc_name}')
        self.proc_name: str = proc_name
        self.formal_params: List[Param] = formal_params  # a list of Param nodes
        self.block_node: Block = block_node


Declarations = (VarDecl, ProcedureDecl)


class Program(AST):
    def __init__(self, token: Token, name: str, block: Block):
        super().__init__(token, f'Program: {name}')
        self.name: str = name
        self.block: Block = block


BranchingNodes = (IfThenElse, WhileLoop, RepeatLoop, ProcedureCall)
Tree = (BranchingNodes, Compound)
Endpoints = (Var, ContextVar, Num, Bool)

# For static typing
from Compiler.BlockStack import BasicBlock
