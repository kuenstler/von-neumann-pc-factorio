"""Generators for different code representations of AST"""

from Compiler.ASTNodes import *
from Compiler.ASTNodes import ContextVar
from Compiler.GenericASTVisitors import GenericVisitorMethods, NodeVisitor


def indent(string: str, indentation: int = 4) -> str:
    return '\n'.join([f'{" " * indentation}{i}' for i in string.splitlines()])


class PascalGenerator(GenericVisitorMethods, NodeVisitor):
    def visit_UnaryOp(self, node: UnaryOp, **kwargs):
        return f'{node.token.type.value} {self.visit(node.expr)}'

    def visit_Bool(self, node: Bool, **kwargs):
        if node.value:
            return 'true'
        return 'false'

    def visit_Break(self, node: Break, *kwargs):
        return 'break'

    def visit_Num(self, node: Num, **kwargs):
        return str(node.value)

    def visit_BinOp(self, node: BinOp, **kwargs):
        left = self.visit(node.left, **kwargs)
        right = self.visit(node.right, **kwargs)
        return f'{left} {node.token.type.value} {right}'

    def visit_Var(self, node: Var, **kwargs):
        return node.value

    def visit_ContextVar(self, node: ContextVar, **kwargs):
        return self.visit_Var(node, **kwargs)

    def visit_Assign(self, node: Assign, **kwargs):
        left = self.visit(node.left, **kwargs)
        right = self.visit(node.right, **kwargs)
        return f'{left} := {right}'

    def visit_RepeatLoop(self, node: RepeatLoop, **kwargs):
        statement = indent(self.visit(node.statement, **kwargs))
        expr = self.visit(node.expr, **kwargs)
        return f'repeat\n{statement}\nuntil {expr}'

    def visit_WhileLoop(self, node: WhileLoop, **kwargs):
        expr = self.visit(node.expr, **kwargs)
        statement = indent(self.visit(node.statement, **kwargs))
        return f'while {expr} do\n{statement}'

    def visit_IfThenElse(self, node: IfThenElse, **kwargs):
        expr = self.visit(node.expr, **kwargs)
        then_statement = indent(self.visit(node.then_statement, **kwargs))
        else_statement = '\nelse\n' + indent(self.visit(node.else_statement)) or ''
        return f'if {expr} then\n{then_statement}{else_statement}'

    def visit_Program(self, node: Program, **kwargs):
        name = node.name
        block = self.visit(node.block, **kwargs)
        return f'program {name};\n{block}.'

    def visit_Block(self, node: Block, **kwargs):
        declarations = ''
        for i in node.declarations:
            if isinstance(i, VarDecl):
                if not declarations:
                    declarations = 'var\n'
                declarations += indent(self.visit(i, **kwargs)) + ';\n'
            else:
                declarations += self.visit(i) + ':\n'
        compound = self.visit(node.compound_statement, **kwargs)
        return f'{declarations}{compound}'

    def visit_BuiltinFunction(self, node: BuiltinFunction, **kwargs):
        arguments = [self.visit(i, **kwargs) for i in node.arguments]
        if node.token.type == TokenType.READLN:
            return f'readln({arguments[0]}, {arguments[1]})'
        elif node.token.type == TokenType.WRITELN:
            return f'writeln({arguments[0]}, {arguments[1]})'
        elif node.token.type == TokenType.WRITEASCII:
            return f'writeascii({arguments[0]}, {arguments[1]})'
        else:
            raise NotImplementedError(f'Invalid builtinfunction {node}')

    def visit_Compound(self, node: Compound, **kwargs):
        statements = ';\n'.join([indent(self.visit(i, **kwargs)) for i in node.children])
        return f'begin\n{statements}\nend'

    def visit_NoOp(self, node: NoOp, **kwargs):
        return ''

    def visit_Type(self, node: Type, **kwargs):
        return node.value.lower()

    def visit_VarDecl(self, node: VarDecl, **kwargs):
        var = self.visit(node.var_node, **kwargs)
        var_type = self.visit(node.type_node, **kwargs)
        return f'{var} : {var_type}'
