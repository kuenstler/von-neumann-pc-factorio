"""Utilities for SPI"""

from inspect import signature, Signature
from typing import List

from Compiler.Options import Options


def convert_number_to(number: int, base: int):
    first_bit = 1 << (base - 1)
    all_bits = (1 << base) - 1
    if number >= 0:
        if number & first_bit == first_bit:
            number = ((number ^ all_bits) + 1) * -1
        return number
    return number & all_bits


def generic_str(self, ignored_keywords: List[str] = None, explicit_keywords: List[str] = None):
    newlines = Options.should_use_newlines
    newline_or_space = '\n' if newlines else ' '
    newline_or_nothing = '\n' if newlines else ''
    string = f'{self.__class__.__name__}({newline_or_nothing}'
    dir = []
    self_signature: Signature = signature(self.__init__)
    ignored_keywords = (ignored_keywords or []) + ['self']
    for i in self_signature.parameters:
        if i not in ignored_keywords:
            dir.append(i)
    if explicit_keywords:
        for i in explicit_keywords:
            if i not in self_signature:
                dir.append(i)
    if dir:
        for i in dir:
            string += f'{"    " if newlines else ""}{i}='
            lines = repr(self.__getattribute__(i)).splitlines()
            string += f'{lines.pop(0)}{newline_or_space}'
            for i in lines:
                string += f'{"    "}{i}{newline_or_space}'
            string = f'{string[:-1]},{newline_or_space}'
        string = string[:-1]
    return f'{string[:-1]}{newline_or_nothing})'
