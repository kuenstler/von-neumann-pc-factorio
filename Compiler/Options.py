"""Options for SPI"""
from io import TextIOWrapper
from typing import ClassVar


class Options:
    should_log_scope: ClassVar[bool] = False  # see '--scope' command line option
    should_log_stack: ClassVar[bool] = False  # see '--stack' command line option
    should_log_optimisations: ClassVar[bool] = False  # see '--optimisations' command line option
    should_use_newlines: ClassVar[bool] = True  # see '--no-newlines' command line option
    show_final_ast: ClassVar[bool] = False  # see '--show-final-ast' command line option
    input_file: ClassVar[TextIOWrapper] = ''
    python_file: ClassVar[TextIOWrapper] = ''
    pascal_file: ClassVar[TextIOWrapper] = ''
    bp_file: ClassVar[TextIOWrapper] = ''
    ast_file: ClassVar[TextIOWrapper] = ''
    verbosity: ClassVar[bool] = False
    bp_integers: ClassVar[bool] = False

    @staticmethod
    def verbose():
        if Options.verbosity:
            for i in (
                    'should_log_scope',
                    'should_log_stack',
                    'should_log_optimisations',
                    'show_final_ast',
                    'bp_integers'
            ):
                setattr(Options, i, True)
