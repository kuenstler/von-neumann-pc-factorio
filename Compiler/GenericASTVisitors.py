"""Generic AST visitors for SPI"""

from Compiler.ASTNodes import *
from Compiler.Tokens import TokenType
from Compiler.Utilities import convert_number_to


class NodeVisitor:
    def visit(self, node, **kwargs):
        method_name = 'visit_' + type(node).__name__
        visitor = getattr(self, method_name, self.generic_visit)
        return visitor(node, **kwargs)

    def generic_visit(self, node, **kwargs):
        raise Exception('No visit_{} method'.format(type(node).__name__))


class GenericVisitorMethods(NodeVisitor):
    def visit_Block(self, node: Block, **kwargs):
        for declaration in node.declarations:
            self.visit(declaration, **kwargs)
        self.visit(node.compound_statement, **kwargs)

    def visit_Break(self, node: Break, **kwargs):
        pass

    def visit_Program(self, node: Program, **kwargs):
        self.visit(node.block, **kwargs)

    def visit_Compound(self, node: Compound, **kwargs):
        for child in node.children:
            self.visit(child, **kwargs)

    def visit_NoOp(self, node: NoOp, **kwargs):
        pass

    def visit_BinOp(self, node: BinOp, **kwargs):
        self.visit(node.left, **kwargs)
        self.visit(node.right, **kwargs)

    def visit_IfThenElse(self, node: IfThenElse, **kwargs):
        self.visit(node.expr, **kwargs)
        self.visit(node.then_statement, **kwargs)
        self.visit(node.else_statement, **kwargs)

    def visit_WhileLoop(self, node: WhileLoop, **kwargs):
        self.visit(node.expr, **kwargs)
        self.visit(node.statement, **kwargs)

    def visit_RepeatLoop(self, node: RepeatLoop, **kwargs):
        self.visit(node.statement, **kwargs)
        self.visit(node.expr, **kwargs)

    def visit_ProcedureDecl(self, node: ProcedureDecl, **kwargs):
        self.visit(node.block_node, **kwargs)

    def visit_VarDecl(self, node: VarDecl, **kwargs):
        pass

    def visit_Assign(self, node: Assign, **kwargs):
        # right-hand side
        self.visit(node.right, **kwargs)
        # left-hand side
        self.visit(node.left, **kwargs)

    def visit_Var(self, node: Var, **kwargs):
        pass

    def visit_Num(self, node: Num, **kwargs):
        pass

    def visit_UnaryOp(self, node: UnaryOp, **kwargs):
        self.visit(node.expr, **kwargs)

    def visit_Bool(self, node: Bool, **kwargs):
        pass

    def visit_ProcedureCall(self, node: ProcedureCall, **kwargs):
        for param_node in node.actual_params:
            self.visit(param_node, **kwargs)

    def visit_BuiltinFunction(self, node: BuiltinFunction, **kwargs):
        for param_node in node.arguments:
            self.visit(param_node, **kwargs)

    def visit_Type(self, node: Type, **kwargs):
        pass

    def visit_ContextVar(self, node: ContextVar, **kwargs):
        self.visit_Var(node, **kwargs)


class NodeVisitorWithOperations(GenericVisitorMethods, NodeVisitor):
    def calculate_binop(self, node: BinOp):
        if node.token.type == TokenType.PLUS:
            return self.visit(node.left) + self.visit(node.right)
        elif node.token.type == TokenType.MINUS:
            return self.visit(node.left) - self.visit(node.right)
        elif node.token.type == TokenType.MUL:
            return self.visit(node.left) * self.visit(node.right)
        elif node.token.type == TokenType.INTEGER_DIV:
            return self.visit(node.left) // self.visit(node.right)
        elif node.token.type == TokenType.FLOAT_DIV:
            return float(self.visit(node.left)) / float(self.visit(node.right))
        elif node.token.type == TokenType.MOD:
            return self.visit(node.left) % self.visit(node.right)
        elif node.token.type == TokenType.EQUAL:
            return self.visit(node.left) == self.visit(node.right)
        elif node.token.type == TokenType.GREATER:
            return self.visit(node.left) > self.visit(node.right)
        elif node.token.type == TokenType.LESS:
            return self.visit(node.left) < self.visit(node.right)
        elif node.token.type == TokenType.AND:
            return self.visit(node.left) & self.visit(node.right)
        elif node.token.type == TokenType.OR:
            return self.visit(node.left) | self.visit(node.right)
        elif node.token.type == TokenType.XOR:
            return self.visit(node.left) ^ self.visit(node.right)
        elif node.token.type == TokenType.BIT_RIGHT_SHIFT:
            return self.visit(node.left) >> self.visit(node.right)
        elif node.token.type == TokenType.BIT_LEFT_SHIFT:
            return self.visit(node.left) << self.visit(node.right)
        elif node.token.type == TokenType.POW:
            return self.visit(node.left) ** self.visit(node.right)
        elif node.token.type == TokenType.GREATER_EQUAL:
            return self.visit(node.left) >= self.visit(node.right)
        elif node.token.type == TokenType.LESS_EQUAL:
            return self.visit(node.left) <= self.visit(node.right)
        elif node.token.type == TokenType.NOT_EQUAL:
            return self.visit(node.left) != self.visit(node.right)
        elif node.token.type == TokenType.MODULO:
            return self.visit(node.left) % self.visit(node.right)

    def calculate_unaryop(self, node: UnaryOp):
        token = node.token.type
        if token == TokenType.PLUS:
            return self.visit(node.expr)
        elif token == TokenType.MINUS:
            return -self.visit(node.expr)
        elif token == TokenType.NOT:
            return not self.visit(node.expr)
        elif token == TokenType.BIT_NOT:
            expr = self.visit(node.expr)
            expr = expr ^ 0xffffffff
            return convert_number_to(expr, 32)
