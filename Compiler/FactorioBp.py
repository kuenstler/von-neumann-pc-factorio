from base64 import b64encode, b64decode
from json import dumps as json_dumps
from typing import TypedDict, List
from zlib import compress as zlib_compress, decompress as zlib_decompress


class Signal(TypedDict):
    type: str
    name: str


class Icon(TypedDict):
    signal: Signal
    index: int


class Position(TypedDict):
    x: int
    y: int


class Filter(TypedDict):
    signal: Signal
    count: int
    index: int


class ControlBehavior(TypedDict):
    filters: List[Filter]


class Entity(TypedDict):
    entity_number: int
    name: str
    position: Position
    control_behavior: ControlBehavior


class Blueprint(TypedDict):
    icons: List[Signal]
    entities: List[Entity]
    item: str
    label: str
    version: int


class Generic(TypedDict):
    blueprint: Blueprint


basic_blueprint: Generic = {
    "blueprint": {
        "icons": [
            {"signal": {"type": "item", "name": "constant-combinator"}, "index": 1}
        ],
        "entities": [],
        "item": "blueprint",
        "label": "",
        "version": 77310197760
    }
}


def list_to_json(input_list: List, name: str) -> str:
    current_bp = basic_blueprint.copy()
    current_bp['blueprint']['label'] = name
    for i in range(len(input_list)):
        row = (i + 1) // 32 * 3
        column = (i + 1) % 32
        current_combinator: Entity = {
            "entity_number": i,
            "name": "constant-combinator",
            "position": {"x": column, "y": row},
            "control_behavior": {
                "filters": [{
                    "signal": {"type": "virtual", "name": "signal-D"},
                    "count": input_list[i],
                    "index": 1
                }]
            }
        }
        current_bp['blueprint']['entities'].append(current_combinator)
    return json_dumps(current_bp, separators=(',', ':'))


def json_to_bp(json: str) -> str:
    compressed: bytes = zlib_compress(json.encode())
    encoded: bytes = b64encode(compressed)
    return '0' + encoded.decode()


def bp_to_json(bp: str) -> str:
    decoded: bytes = b64decode(bp[1:].encode())
    uncompressed: bytes = zlib_decompress(decoded)
    return uncompressed.decode()
