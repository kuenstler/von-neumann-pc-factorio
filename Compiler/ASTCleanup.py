"""AST Cleanup for SPI AST"""
from typing import Dict

from Compiler.ASTNodes import *
from Compiler.GenericASTVisitors import NodeVisitorWithOperations
from Compiler.Options import Options
from Compiler.Tokens import TokenType

Environment = Dict[str, bool]


class ASTCleanup(NodeVisitorWithOperations):
    def truth_visit(self, node):
        method_name = 'truth_visit_' + type(node).__name__
        visitor = getattr(self, method_name, self.generic_visit)
        return visitor(node)

    def treat(self, node, name: str, environment: Environment = None):
        is_list = '.' in name
        if is_list:
            list_name = name.split('.')[0]
            list_index = int(name.split('.')[1])
            _list = node.__getattribute__(list_name)
            treat_node = _list[list_index]
        else:
            treat_node = node.__getattribute__(name)
        method_name = 'treat_' + type(treat_node).__name__
        visitor = getattr(self, method_name, None)
        if visitor:
            new_node = visitor(treat_node, environment=environment or {})
            if is_list:
                if type(new_node) == NoOp:
                    _list.remove(treat_node)
                    changed = True
                else:
                    _list[list_index] = new_node
                    changed = False
                node.__setattr__(list_name, _list)
                return changed
            node.__setattr__(name, new_node)

    def log(self, msg):
        if Options.should_log_optimisations:
            print(msg)

    def treat_BinOp(self, node: BinOp, environment: Environment):
        if environment.get('NoVar'):
            self.log(f'Calculating\n{node}\nwithout variables')
            result = self.calculate_binop(node)
            new_node = Num(
                token=node.token,
                value=result
            )
            return new_node
        return node

    def treat_IfThenElse(self, node: IfThenElse, environment: Environment):
        if node.else_statement == node.then_statement:
            self.log(f'Removing\n{node}\nwith equal then and else statements')
            return node.then_statement
        if environment.get('NoVar'):
            expr = self.visit(node.expr)
            if expr:
                self.log(f'Converting always reached\n{node}')
                return node.then_statement
            if not expr:
                self.log(f'Removing unreachable\n{node}')
                return node.else_statement
        return node

    def treat_WhileLoop(self, node: WhileLoop, environment: Environment):
        if environment.get('NoVar'):
            if not self.visit(node.expr):
                self.log(f'Removing unreachable\n{node}')
                return NoOp(node.token)
            else:
                self.log(f'Simplifying always True\n{node}')
                node.expr = Bool(node.expr.token, value=1)
        return node

    def treat_RepeatLoop(self, node: RepeatLoop, environment: Environment):
        if environment.get('NoVar'):
            if not self.visit(node.expr):
                self.log(f'Converting once-reached\n{node}')
                return node.statement
            else:
                self.log(f'Simplifying always True\n{node}')
                node.expr = Bool(node.expr.token, value=1)
        return node

    def truth_visit_Block(self, node: Block):
        for declaration in node.declarations:
            self.truth_visit(declaration)
        return self.truth_visit(node.compound_statement)

    def truth_visit_Program(self, node: Program):
        return self.truth_visit(node.block)

    def truth_visit_Compound(self, node: Compound):
        return_list = []
        change = 0
        for child in range(len(node.children)):
            return_list.append(self.truth_visit(node.children[child + change]))
            if self.treat(node, f'children.{child + change}', {'NoVar': return_list[-1]}):
                change -= 1
                return_list.pop(-1)
        return all(return_list)

    def truth_visit_NoOp(self, node: NoOp):
        return True

    def truth_visit_Break(self, node: Break):
        return True

    def truth_visit_BinOp(self, node: BinOp):
        left = self.truth_visit(node.left)
        if isinstance(node.left, UnaryOp) and node.left.token.type == TokenType.PLUS:
            self.log(f'Cleaning up positive\n{node.left}')
            node.left = node.left.expr
        elif (isinstance(node.left, UnaryOp)
              and node.left.token.type == TokenType.MINUS
              and node.token.type == TokenType.MINUS
        ):
            self.log(f'Cleaning up negative\n{node.left}\nin combination with negative\n{node}')
            node.token.type = TokenType.PLUS
            node.left = node.left.expr
        self.treat(node, 'left', {'NoVar': left})
        right = self.truth_visit(node.right)
        if isinstance(node.right, UnaryOp) and node.right.token.type == TokenType.PLUS:
            self.log(f'Cleaning up positive\n{node.right}')
            node.right = node.right.expr
        elif (isinstance(node.right, UnaryOp)
              and node.right.token.type == TokenType.MINUS
              and node.token.type == TokenType.MINUS
        ):
            self.log(f'Cleaning up negative\n{node.right}\nin combination with negative\n{node}')
            node.token.type = TokenType.PLUS
            node.right = node.right.expr
        self.treat(node, 'right', {'NoVar': right})
        return left and right

    def visit_BinOp(self, node: BinOp, **kwargs):
        return self.calculate_binop(node)

    def truth_visit_IfThenElse(self, node: IfThenElse):
        expr = self.truth_visit(node.expr)
        self.treat(node, 'expr', {'NoVar': expr})
        then_statement = self.truth_visit(node.then_statement)
        else_statement = self.truth_visit(node.else_statement)
        return expr and then_statement and else_statement

    def truth_visit_WhileLoop(self, node: WhileLoop):
        expr = self.truth_visit(node.expr)
        self.treat(node, 'expr', {'NoVar': expr})
        statement = self.truth_visit(node.statement)
        return expr and statement

    def truth_visit_RepeatLoop(self, node: RepeatLoop):
        statement = self.truth_visit(node.statement)
        expr = self.truth_visit(node.expr)
        self.treat(node, 'expr', {'NoVar': expr})
        return statement and expr

    def truth_visit_ProcedureDecl(self, node: ProcedureDecl):
        return self.truth_visit(node.block_node)

    def truth_visit_VarDecl(self, node: VarDecl):
        pass

    def truth_visit_Assign(self, node: Assign):
        # right-hand side
        right = self.truth_visit(node.right)
        self.treat(node, 'right', {'NoVar': right})
        # left-hand side
        self.truth_visit(node.left)
        return right

    def visit_Assign(self, node: Assign, **kwargs):
        raise KeyError(f'Tried to access {node} during optimisations')

    def truth_visit_Var(self, node: Var):
        return False

    def visit_Var(self, node: Var, **kwargs):
        raise KeyError(f'Tried to access {node} during optimisations')

    def truth_visit_Num(self, node: Num):
        return True

    def visit_Num(self, node: Num, **kwargs):
        return node.value

    def truth_visit_UnaryOp(self, node: UnaryOp):
        while (isinstance(node.expr, UnaryOp)
               and node.token.type in (TokenType.MINUS, TokenType.PLUS)
               and node.expr.token.type in (TokenType.MINUS, TokenType.PLUS)
        ):
            own = node.token.type == TokenType.PLUS
            foreign = node.expr.token.type == TokenType.PLUS
            result = False
            if own and foreign or not own and not foreign:
                result = True
            if result and not own:
                node.token.type = TokenType.PLUS
            elif not foreign:
                node.token = node.expr.token
            self.log(f'Cleaning up double\n{node}')
            node.expr = node.expr.expr
        return self.truth_visit(node.expr)

    def visit_UnaryOp(self, node: UnaryOp, **kwargs):
        return self.calculate_unaryop(node)

    def truth_visit_Bool(self, node: Bool):
        return True

    def visit_Bool(self, node: Bool, **kwargs):
        return node.value

    def visit_Break(self, node: Break, **kwargs):
        pass

    def truth_visit_ProcedureCall(self, node: ProcedureCall):
        return_list = []
        change = 0
        for param_node in range(len(node.actual_params)):
            return_list.append(self.truth_visit(node.actual_params[param_node + change]))
            if self.treat(node, f'actual_params.{param_node + change}', {'NoVar': return_list[-1]}):
                change -= 1
                return_list.pop(-1)
        return all(return_list)

    def truth_visit_BuiltinFunction(self, node: BuiltinFunction):
        return_list = []
        change = 0
        for param_node in range(len(node.arguments)):
            return_list.append(self.truth_visit(node.arguments[param_node + change]))
            if self.treat(node, f'arguments.{param_node + change}', {'NoVar': return_list[-1]}):
                change -= 1
                return_list.pop(-1)
        return all(return_list)
