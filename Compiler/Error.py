from enum import Enum


class ErrorCode(str, Enum):
    UNEXPECTED_TOKEN = 'Unexpected token'
    ID_NOT_FOUND = 'Identifier not found'
    DUPLICATE_ID = 'Duplicate id found'
    BREAK = 'Break outside of loop'


class Error(Exception):
    def __init__(self, error_code: str = None, token=None, message: str = None):
        self.error_code: str = error_code
        self.token = token
        # add exception class name before the message
        self.message = f'{self.__class__.__name__}: {message}'


class LexerError(Error):
    pass


class ParserError(Error):
    pass


class SemanticError(Error):
    pass
