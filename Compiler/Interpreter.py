"""Interpreter for SPI"""
from typing import Dict
import sys

from Compiler.ASTNodes import *
from Compiler.ActivationRecord import ActivationRecord, ARType
from Compiler.CallStack import CallStack
from Compiler.GenericASTVisitors import NodeVisitorWithOperations
from Compiler.Options import Options
from Compiler.Tokens import TokenType

colors: Dict[int, str] = {0: 'white', 1: 'red', 2: 'green', 3: 'blue', 4: 'yellow', 5: 'magenta',
                          6: 'cyan', 7: 'grey', 8: 'black'}

matrix_colors: Dict[int, str] = {
    0: '\033[49m   ',
    1: '\033[47m   ',
    2: '\033[41m   ',
    3: '\033[42m   ',
    4: '\033[44m   ',
    5: '\033[43m   ',
    6: '\033[45m   ',
    7: '\033[46m   ',
}

def single_input(msg: str) -> str:
    return input(msg)

class Interpreter(NodeVisitorWithOperations):
    def __init__(self, tree, arguments: Dict[int, int] = None, step: bool = False):
        self.tree = tree
        self.arguments: Dict[int, int] = arguments or {}
        self.call_stack = CallStack()
        self.step = step
        self.matrix = [0 for i in range(32)]

    def log(self, msg: str):
        if Options.should_log_stack:
            print(msg)

    def inspect_variables(self):
        ar = self.call_stack.peek()
        while True:
            key = single_input('\nWhat do you wnat to do with variables?')
            print()
            if key == '?':
                print (
"""
Variable assistant.

?: this help
c: continue execution
l: list all variables and display scope information
v: inspect specific variable
"""
                    )
            elif key == 'c':
                break
            elif key == 'l':
                print(ar)
            elif key == 'v':
                name = input('Enter the name of the variable')
                print(f'{name}: {ar[name]}')

    def step_menu(self, node):
        if self.step:
            while True:
                key = single_input(f'What do you want to do at {node.name}?')
                if key == '?':
                    print (
"""
Stepping assistant. Stepping in and out is currently not supported

?: this help
c: continue execution
n: view full node AST represenatation
v: inspect variables
"""
                    )
                elif key == 'c':
                    print()
                    break
                elif key == 'n':
                    print(node)
                elif key == 'v':
                    self.inspect_variables()

    def visit(self, node, **kwargs):
        self.step_menu(node)
        return super().visit(node, **kwargs)

    def visit_Program(self, node: Program, **kwargs):
        program_name = node.name
        self.log(f'ENTER: PROGRAM {program_name}')

        ar = ActivationRecord(
            name=program_name,
            type=ARType.PROGRAM,
            nesting_level=1,
        )
        self.call_stack.push(ar)

        self.log(str(self.call_stack))

        self.visit(node.block, **kwargs)

        self.log(f'LEAVE: PROGRAM {program_name}')
        self.log(str(self.call_stack))

        self.call_stack.pop()

    def visit_BinOp(self, node: BinOp, **kwargs):
        return self.calculate_binop(node)

    def visit_Num(self, node: Num, **kwargs):
        return node.value

    def visit_Bool(self, node: Bool, **kwargs):
        return node.value

    def visit_UnaryOp(self, node: UnaryOp, **kwargs):
        return self.calculate_unaryop(node)

    def visit_Compound(self, node: Compound, **kwargs):
        for child in node.children:
            if self.visit(child, **kwargs):
                return True

    def visit_Assign(self, node: Assign, **kwargs):
        var_name = node.left.value
        var_value = self.visit(node.right, **kwargs)

        ar = self.call_stack.peek()
        ar[var_name] = var_value

    def visit_Var(self, node: Var, **kwargs):
        var_name = node.value

        ar = self.call_stack.peek()
        var_value = ar.get(var_name)

        assert var_value is not None, f'Var {node} was not set'

        return var_value

    def visit_ContextVar(self, node: ContextVar, **kwargs):
        return self.visit_Var(node, **kwargs)

    def visit_ProcedureCall(self, node: ProcedureCall, **kwargs):
        proc_name = node.proc_name
        proc_symbol = node.proc_symbol

        ar = ActivationRecord(
            name=proc_name,
            type=ARType.PROCEDURE,
            nesting_level=proc_symbol.scope_level + 1,
        )

        formal_params = proc_symbol.formal_params
        actual_params = node.actual_params

        for param_symbol, argument_node in zip(formal_params, actual_params):
            ar[param_symbol.name] = self.visit(argument_node)

        self.call_stack.push(ar)

        self.log(f'ENTER: PROCEDURE {proc_name}')
        self.log(repr(self.call_stack))

        # evaluate procedure body
        self.visit(proc_symbol.block_ast, **kwargs)

        self.log(f'LEAVE: PROCEDURE {proc_name}')
        self.log(repr(self.call_stack))

        self.call_stack.pop()

    def visit_BuiltinFunction(self, node: BuiltinFunction, **kwargs):
        if node.token.type == TokenType.READLN:
            var_name = node.arguments[1].token.value
            var_value = self.arguments.get(self.visit(node.arguments[0]), 0)
            print(f'Read {var_value} from {var_name}')
            ar = self.call_stack.peek()
            ar[var_name] = var_value
        elif node.token.type == TokenType.WRITEASCII:
            address = self.visit(node.arguments[0], **kwargs)
            value = self.visit(node.arguments[1], **kwargs)
            output_type: int = (address >> 5) & 0x7ffff
            cell: int = address & 31
            if output_type == 5:
                print(f'Wrote ASCII character {chr(value)} to ASCII cell {cell}')
            else:
                raise RuntimeError(f'Ascii is only expected on cluster 5 and not {output_type}')
        else:
            address = self.visit(node.arguments[0], **kwargs)
            value = self.visit(node.arguments[1], **kwargs)
            output_type: int = (address >> 5) & 0x7ffff
            cell: int = address & 31
            if output_type == 0:
                print(f'Wrote value {value} to cell {cell}')
            elif output_type == 1:
                print(f'wrote matrix_line {cell + 1:2} "', end='')
                for i in range(31, -1, -1):
                    print('*' if (value >> i) & 1 else ' ', end='')
                print('"')
            elif output_type == 2 and cell == 0:
                print(f'Set matrix color to {colors[value]}')
            elif output_type == 3:
                self.matrix[cell] = value
            elif output_type == 4:
                if cell == 0:
                    self.matrix = [0 for i in range(32)]
                elif cell == 1:
                    row = value | (value << 3) | (value << 6) | (value << 9) | (value << 12) | (value << 15) | (value << 18) | (value << 21) | (value << 24) | (value << 27)
                    self.matrix = [row for i in range(32)]
            elif output_type == 5:
                if cell == 31:
                    print(f'Wrote {value}({chr(value)}) to all ASCII cells')
            else:
                raise RuntimeError(f'Unexpected write to type {output_type} and cell {cell}')

    def visit_IfThenElse(self, node: IfThenElse, **kwargs):
        if self.visit(node.expr, **kwargs):
            return self.visit(node.then_statement, **kwargs)
        else:
            return self.visit(node.else_statement, **kwargs)

    def visit_WhileLoop(self, node: WhileLoop, **kwargs):
        while self.visit(node.expr, **kwargs):
            if self.visit(node.statement, **kwargs):
                break

    def visit_RepeatLoop(self, node: RepeatLoop, **kwargs):
        while True:
            if self.visit(node.statement, **kwargs):
                break
            if not self.visit(node.expr, **kwargs):
                break

    def visit_Break(self, node: Break, **kwargs):
        return True

    def interpret(self, **kwargs):
        tree = self.tree
        if tree is None:
            return ''
        res = self.visit(tree, **kwargs)
        if any(i != 0 for i in self.matrix):
            print('Matrix has been changed, last layout:')
            for (n, i) in enumerate(self.matrix):
                for j in range(0, 30, 3):
                    print(matrix_colors[(i >> j) & 7], end='')
                if n % 2 == 1:
                    print('\033[49m')
        return res

