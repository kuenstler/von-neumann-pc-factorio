"""Compiler for spi from AST to integers"""

from collections import UserList
from typing import TypedDict, Dict, Any

from Compiler.ASTNodes import *
from Compiler.BlockStack import ProgramStackProxy, IGetCOMPVar
from Compiler.GenericASTVisitors import NodeVisitor, GenericVisitorMethods
from Compiler.Tokens import TokenType
from Compiler.Utilities import convert_number_to


class TypeDict(TypedDict):
    type: str


class SetDict(TypeDict):
    number: int


class JumpDict(TypeDict, total=False):
    target: int


class Int32List(UserList):
    def append(self, __object: Union[str, int]) -> None:
        if isinstance(__object, int):
            __object = convert_number_to(__object, 32)
        super().append(__object)


VarMapping = Dict[Var, int]
Pointers = List[TypeDict]
MixedList = 'Int32List[Union[str, int]]'

arithmetic_coms: Dict[TokenType, int] = {
    TokenType.PLUS: 0xc0000000, TokenType.MINUS: 0xc4000000, TokenType.MUL: 0xc8000000,
    TokenType.INTEGER_DIV: 0xcc000000, TokenType.EXPONENT: 0xd0000000, TokenType.MOD: 0xd4000000,
    TokenType.BIT_RIGHT_SHIFT: 0xd8000000, TokenType.BIT_LEFT_SHIFT: 0xdc000000,
    TokenType.AND: 0xe0000000, TokenType.OR: 0xe4000000, TokenType.XOR: 0xe8000000,
    TokenType.BIT_NOT: 0xfe000000, TokenType.NOT: 0xe8000000, TokenType.EQUAL: 0xfc800000,
    TokenType.NOT_EQUAL: 0xfcc00000, TokenType.LESS: 0xfd000000, TokenType.LESS_EQUAL: 0xfd400000,
    TokenType.GREATER: 0xfd800000, TokenType.GREATER_EQUAL: 0xfdc00000, TokenType.MODULO: 0xd4000000
}


class Compiler(GenericVisitorMethods, NodeVisitor, IGetCOMPVar):
    len_regflag: int = 9
    len_reg: int = 4
    len_storage: int = 26

    # upper bounds
    maximum_reg: int = (1 << len_reg) - 1
    maximum_regflag: int = (1 << len_regflag) - 1
    maximum_storage: int = (1 << len_storage) - 1
    maximum_storage_of_type: int = (1 << len_storage - 2) - 1
    maximum_num: int = (1 << len_regflag - 2) - 1
    minimum_num: int = (1 << len_regflag - 2) * -1

    # some addresses
    input_address: int = 2 << len_storage - 2
    output_address: int = 3 << len_storage - 2

    def __init__(self, tree: Program, stack: ProgramStackProxy, vars: VarMapping = None, pointers: Pointers = None):
        super().__init__()
        self._vars: VarMapping = vars or {}
        self.pointers: Pointers = pointers or []
        self.tree: Program = tree
        assert len(self.tree.block.declarations) <= 16, f'More than 16 variables are currently not supported'
        self.stack: ProgramStackProxy = stack
        self.codes: MixedList = Int32List()

    def remove_text(self):
        for i in range(len(self.codes)):
            value: Union[int, str] = self.codes[i]
            if isinstance(value, str):
                if value.startswith('jump'):
                    temp: List[str] = value[5:].split(' if ')
                    hint: TypeDict = self.pointers[int(temp[0])]
                    assert hint['type'] == 'jump'
                    var: int = int(temp[1])
                    instruction: int = (
                            0xf0000000
                            | convert_number_to(hint['target'] - i, 16) << self.len_regflag
                            | var
                    )
                    self.codes[i] = convert_number_to(instruction, 32)
                elif value.startswith('set'):
                    temp: List[str] = value[4:].split(' from ')
                    hint: TypeDict = self.pointers[int(temp[0])]
                    assert hint['type'] == 'set'
                    var: int = int(temp[1])
                    future_pos: int = len(self.codes)
                    self.codes.append(hint['number'])
                    instruction: int = (
                            0xf8000000
                            | convert_number_to(future_pos - i, 16)
                            | var << 16
                    )
                    self.codes[i] = convert_number_to(instruction, 32)
                else:
                    raise RuntimeError(f'unexpected command {value} in remove_text')

    @property
    def vars(self) -> VarMapping:
        return self._vars

    @vars.setter
    def vars(self, value: VarMapping):
        assert len(value) <= 16, f'More than 16 variables are currently not supported'
        self._vars = value

    def compile(self) -> MixedList:
        self.visit(self.tree)
        self.remove_text()
        return self.codes

    def get_regflag_from_int(self, number: int) -> int:
        """number may be any int between -128 and 127, will get converted to a proper number"""
        if not self.minimum_num <= number <= self.maximum_num:
            raise RuntimeError(f'Tried to get regflag from too large number {number}')
        out = 1 << self.len_regflag - 1
        out |= convert_number_to(number, self.len_regflag - 1)
        return out

    def visit_Program(self, node: Program, **kwargs):
        for i in node.block.declarations:
            assert isinstance(i, VarDecl)
            self.vars[i.var_node] = len(self.vars)
        self.visit(node.block.compound_statement, **kwargs)
        self.codes.append(0xf4000000)  # quit

    @staticmethod
    def generate_jump(id: Any, condition: Any) -> str:
        return f'jump {id} if {condition}'

    def visit_WhileLoop(self, node: WhileLoop, **kwargs):
        # get the (future) position of the jump_to_start pointer
        jump_pos: int = len(self.pointers)
        # jump to be placed at the end, pointing to the top of the codes
        jump_to_start: JumpDict = {'type': 'jump', 'target': len(self.codes)}
        # add the jump_to_start pointer to the pointers
        self.pointers.append(jump_to_start)
        # if it is an endless loop, ignore the following
        # create the jump_to_end, to be precised later
        jump_to_end: JumpDict = {'type': 'jump'}
        # add it to the pointers one index after jump_pos
        self.pointers.append(jump_to_end)
        if isinstance(node.expr, Bool):
            """while True do ... loop due to optimisations"""
        else:
            # add the jump_to_end instruction to the program
            self.codes.append(self.generate_jump(jump_pos + 1, self.visit(node.expr, **kwargs)))
        # work on the content
        self.visit(node.statement, **{**kwargs, 'end': jump_pos + 1})
        # add the jump_to_start instruction to the program
        self.codes.append(self.generate_jump(jump_pos, '1'))
        # add the missing target to jump_to_end, it will update the reference in the pointers
        jump_to_end['target'] = len(self.codes)

    def visit_RepeatLoop(self, node: RepeatLoop, **kwargs):
        # get the position to jump to
        jump_to: int = len(self.codes)
        # jump entry to be placed later
        jump_to_end: JumpDict = {'type': 'jump', 'target': jump_to}
        # get the position of the jump entry
        jump_pos: int = len(self.pointers)
        # add the jump
        self.pointers.append(jump_to_end)
        # work on the content
        self.visit(node.statement, **{**kwargs, 'end': jump_pos})
        # add the jump instruction to the program
        self.codes.append(self.generate_jump(jump_pos, self.visit(node.expr, **kwargs)))

    def visit_Var(self, node: Var, **kwargs) -> int:
        return self.vars[node]

    def visit_Num(self, node: Num, **kwargs) -> int:
        if not self.minimum_num <= node.value <= self.maximum_num:
            assign, var = self.get_comp_var(node)
            self.visit(assign)
            return self.visit_Var(var)
        return self.get_regflag_from_int(node.value)

    def visit_Bool(self, node: Bool, **kwargs) -> int:
        return self.get_regflag_from_int(node.value)

    def visit_Assign(self, node: Assign, **kwargs):
        # Check if number is a terminal and if yes, optimise it
        if isinstance(node.right, (Num, Bool)):
            # the value to set
            value: int = node.right.value
            # placeholders
            summand1: int = 0
            summand2: int = 0
            if self.maximum_num >= value >= 0:
                # only one positive summand needed
                summand1 = value
            elif 2 * self.maximum_num >= value >= 0:
                # both positive summands needed
                summand1 = self.maximum_num
                summand2 = value - self.maximum_num
            else:
                # value is too large for add, stored separately
                set_entry: SetDict = {'type': 'set', 'number': value}
                # the position where the set pointer will be stored
                set_pos: int = len(self.pointers)
                # add the set pointer
                self.pointers.append(set_entry)
                # update the kwargs for this visit
                _kwargs = {**kwargs, 'write': True}
                # add the set instruction
                self.codes.append(f'set {set_pos} from {self.visit_Var(node.left, **_kwargs)}')
                # done with this chase
                return
            # get the regflags fot both summands
            summand1: int = self.get_regflag_from_int(summand1)
            summand2: int = self.get_regflag_from_int(summand2)
            # get the target register
            target: int = self.visit_Var(node.left, **{**kwargs, 'write': True})
            # combine everything
            addition: int = (
                    arithmetic_coms[TokenType.PLUS]
                    | target << self.len_regflag * 2
                    | summand1 << self.len_regflag
                    | summand2
            )
            # add the add instruction to the codes, will get casted later
            self.codes.append(addition)
        elif isinstance(node.right, Var):
            command: int = arithmetic_coms[TokenType.PLUS]
            summand1: int = self.get_regflag_from_int(0)
            summand2: int = self.visit_Var(node.right, **kwargs)
            target: int = self.visit_Var(node.left, **{**kwargs, 'write': True})
            instruction: int = (
                    command
                    | target << self.len_regflag * 2
                    | summand2 << self.len_regflag
                    | summand1
            )
            self.codes.append(instruction)
        elif isinstance(node.right, BinOp):
            # treat BinOps
            # get the target register
            target: int = self.visit_Var(node.left, **{**kwargs, 'write': True}) << self.len_regflag * 2
            # get the base instruction using the token type
            base: int = arithmetic_coms[node.right.token.type]
            # get the value for the left and right arm, if num, this is already correct for the compiler
            left: int = self.visit(node.right.left, **kwargs)
            right: int = self.visit(node.right.right, **kwargs) << self.len_regflag
            # combine the instruction
            instruction: int = (
                    base
                    | target
                    | right
                    | left
            )
            self.codes.append(instruction)
        elif isinstance(node.right, UnaryOp):
            if isinstance(node.right.expr, Num) and node.right.token.type == TokenType.MINUS:
                value: int = node.right.expr.value * -1
                if self.minimum_num <= value < 0:
                    # only one negative summand needed
                    summand1 = value
                    summand2 = 0
                elif 2 * self.minimum_num <= value < 0:
                    # both negative summands needed
                    summand1 = self.minimum_num
                    summand2 = value - self.minimum_num
                else:
                    # value is too large for add, stored separately
                    set_entry: SetDict = {'type': 'set', 'number': value}
                    # the position where the set pointer will be stored
                    set_pos: int = len(self.pointers)
                    # add the set pointer
                    self.pointers.append(set_entry)
                    # update the kwargs for this visit
                    _kwargs = {**kwargs, 'write': True}
                    # add the set instruction
                    self.codes.append(f'set {set_pos} from {self.visit_Var(node.left, **_kwargs)}')
                    # done with this chase
                    return
                # get the regflags fot both summands
                summand1: int = self.get_regflag_from_int(summand1)
                summand2: int = self.get_regflag_from_int(summand2)
                # get the target register
                target: int = self.visit_Var(node.left, **{**kwargs, 'write': True})
                # combine everything
                addition: int = (
                        arithmetic_coms[TokenType.PLUS]
                        | target << self.len_regflag * 2
                        | summand1 << self.len_regflag
                        | summand2
                )
                # add the add instruction to the codes, will get casted later
                self.codes.append(addition)
            else:
                # get the target register
                target: int = self.visit_Var(node.left, **{**kwargs, 'write': True}) << self.len_regflag * 2
                # get the base instruction using the token type
                base: int = arithmetic_coms[node.right.token.type]
                if node.right.token.type == TokenType.NOT:
                    # treat NOT, because this is actually a BinOp with the second side at 0
                    base |= self.get_regflag_from_int(1) << self.len_regflag
                # get the value for the expr, if num, this is already correct for the compiler
                expr: int = self.visit(node.right.expr)
                # combine the instruction
                instruction: int = (
                        base
                        | target
                        | expr
                )
                self.codes.append(instruction)
        else:
            # shouldn't happen
            raise NotImplementedError(f'Unhandled exception in visit_Assign for {node}')

    def visit_Break(self, node: Break, **kwargs):
        target: int = kwargs.get('end')
        if target is None:
            raise RuntimeError(f'visit_Break encountered a empty target')
        self.codes.append(self.generate_jump(target, self.get_regflag_from_int(1)))

    def visit_BuiltinFunction(self, node: BuiltinFunction, **kwargs):
        # either a register or regflag
        register: int = self.visit(node.arguments[1], **kwargs)
        read_from_regflag: bool = (
                isinstance(node.arguments[0], Var)
                or register > self.maximum_reg
        )
        if read_from_regflag:
            address: int = self.visit(node.arguments[0], **kwargs)
        else:
            address: int = node.arguments[0].value
            assert 0 <= address <= self.maximum_storage, f'address is too large {address}'
        if node.token.type == TokenType.WRITELN:
            if read_from_regflag:
                # address may change, using from_write_to
                command: int = 0xfc400000
                instruction: int = (
                        command
                        | address << self.len_regflag
                        | register
                )
            else:
                command: int = 0x80000000
                assert 0 <= register <= self.maximum_reg, f'register is not a register {register}'
                instruction: int = (
                        command
                        | address << self.len_reg
                        | register
                )
        elif node.token.type == TokenType.READLN:
            assert register <= self.maximum_reg
            if read_from_regflag:
                command = 0xfc000000
                instruction: int = (
                        command
                        | address << self.len_regflag
                        | self.get_regflag_from_int(register)
                )
            else:
                command = 0x40000000
                instruction: int = (
                        command
                        | address
                        | register << self.len_storage
                )
        elif node.token.type == TokenType.WRITEASCII:
            assert register <= self.maximum_regflag
            command = 0xfe400000
            instruction: int = (
                command
                | address << self.len_regflag
                | register
            )
        else:
            raise RuntimeError(f'Unexpected BuiltinFunction {node.token.type}')
        self.codes.append(instruction)

    def visit_IfThenElse(self, node: IfThenElse, **kwargs):
        # the jump pos for the end, + 1 for then
        jump_pos: int = len(self.pointers)
        # the jump to the end
        jump_to_end: JumpDict = {'type': 'jump'}
        self.pointers.append(jump_to_end)
        # the jump to the then statement
        jump_to_then: JumpDict = {'type': 'jump'}
        self.pointers.append(jump_to_then)
        # add the jump to then to the codes
        self.codes.append(self.generate_jump(jump_pos + 1, self.visit(node.expr, **kwargs)))
        if not isinstance(node.else_statement, NoOp):
            # proceed with the else statement
            self.visit(node.else_statement, **kwargs)
        # skip the then part
        self.codes.append(self.generate_jump(jump_pos, self.get_regflag_from_int(1)))
        jump_to_then['target'] = len(self.codes)
        self.visit(node.then_statement, **kwargs)
        jump_to_end['target'] = len(self.codes)
