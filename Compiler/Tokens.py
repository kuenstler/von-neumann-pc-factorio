from enum import Enum


class TokenType(str, Enum):
    # single-character token types
    PLUS = '+'
    MINUS = '-'
    MUL = '*'
    FLOAT_DIV = '/'
    LPAREN = '('
    RPAREN = ')'
    SEMI = ';'
    DOT = '.'
    COLON = ':'
    COMMA = ','
    EQUAL = '='
    GREATER = '>'
    LESS = '<'
    AND_ALT = '&'
    OR_ALT_1 = '|'
    BIT_NOT = '~'
    OR_ALT_2 = '!'
    MODULO = '%'
    # block of reserved words
    PROGRAM = 'PROGRAM'  # marks the beginning of the block
    INTEGER = 'INTEGER'
    REAL = 'REAL'
    INTEGER_DIV = 'DIV'
    VAR = 'VAR'
    PROCEDURE = 'PROCEDURE'
    BEGIN = 'BEGIN'
    IF = 'IF'
    THEN = 'THEN'
    ELSE = 'ELSE'
    WHILE = 'WHILE'
    DO = 'DO'
    REPEAT = 'REPEAT'
    UNTIL = 'UNTIL'
    AND = 'AND'
    OR = 'OR'
    NOT = 'NOT'
    XOR = 'XOR'
    BIT_LEFT_SHIFT_ALT = 'SHL'
    BIT_RIGHT_SHIFT_ALT = 'SHR'
    MOD = 'MOD'
    POW = 'POW'
    TRUE = 'TRUE'
    FALSE = 'FALSE'
    WRITELN = 'WRITELN'
    WRITEASCII = 'WRITEASCII'
    READLN = 'READLN'
    BREAK = 'BREAK'
    END = 'END'  # marks the end of the block
    # misc
    ID = 'ID'
    INTEGER_CONST = 'INTEGER_CONST'
    ASSIGN = ':='
    GREATER_EQUAL = '>='
    LESS_EQUAL = '<='
    EXPONENT = '**'
    BIT_LEFT_SHIFT = '<<'
    BIT_RIGHT_SHIFT = '>>'
    NOT_EQUAL = '<>'
    EOF = 'EOF'


class Token:
    def __init__(self, type: TokenType, value: [str, int, bool], lineno: int = None, column: int = None,
                 first: bool = False):
        self._type: TokenType = type
        self._value: str = value
        self.lineno: int = lineno
        self.column: int = column
        self.first: bool = first

    def __hash__(self):
        return hash((self.type, self.value))

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value: TokenType):
        self._type = value
        if value not in (TokenType.ID, TokenType.INTEGER_CONST):
            self._value = value.value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value: str):
        self._value = value
        try:
            self._type = TokenType(value)
        except ValueError:
            if isinstance(value, int):
                self._type = TokenType.INTEGER_CONST
            else:
                self._type = TokenType.ID

    def __eq__(self, other):
        if not isinstance(other, Token):
            return False
        return self.type == other.type

    def __str__(self):
        """String representation of the class instance.

        Example:
            >>> Token(TokenType.INTEGER, 7, lineno=5, column=10)
            Token(TokenType.INTEGER, 7, position=5:10)
        """
        return f'Token({self.type!s}, {self.value!r}, lineno={self.lineno}, column={self.column}, first={self.first})'

    def __repr__(self):
        return self.__str__()
